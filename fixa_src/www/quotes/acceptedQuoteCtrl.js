'use strict';

angular.module("MyApp.controllers").controller("QuoteCtrl", 
	['$scope', '$cordovaLaunchNavigator', 'QuoteData', 'FIREBASE_ROOT', '$firebaseObject', "$cordovaGeolocation", '$ionicLoading', '$state', '$stateParams',
		function($scope, $cordovaLaunchNavigator, QuoteData, FIREBASE_ROOT, $firebaseObject, $cordovaGeolocation, $ionicLoading, $state, $stateParams){

	var ref = new Firebase(FIREBASE_ROOT);
	var user = ref.getAuth();
	var posOptions = {timeout: 10000, enableHighAccuracy: true};
	if($stateParams) {
		$scope.jobKey = $stateParams.key;
	}
	if (user) {
		$ionicLoading.show({
			template: 'Loading...'
		});

		$scope.user = user;
	  var user_uid  = user.uid.substring(12);
		$scope.uid = user_uid;
		var userRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
		var userRefObj = $firebaseObject(userRef);		
		userRefObj.$loaded(function(data){
			$scope.contractorStatus = data.contractorStatus; 
		})
		.then(function(){
			$cordovaGeolocation
			  .getCurrentPosition(posOptions)
			  .then(function (position) {
			    $scope.userLat  = position.coords.latitude;
			    $scope.userLng = position.coords.longitude;			    
			  }, function(err) {
			    console.log(err);
			  });

		  QuoteData.getDetails().then(function(data){	  	
		  	$scope.bidder_id = data.bidder_id;	
	  		var requesterRef = new Firebase(FIREBASE_ROOT+'/users/'+data.requester_id);
	  		$scope.requesterObj = $firebaseObject(requesterRef);
	  		$scope.requesterObj.$loaded(function(data){
	  			$scope.requester_profile_pic = data.profile_pic;
	  		})

	  		if ($scope.contractorStatus) {
	  			$scope.requesterName = data.requester_name;
	  		} else {
	  			$scope.proName = data.proUserDetails.first_name + " " + data.proUserDetails.last_name; 
	  		}
	  		
	  		$scope.proPic = data.proUserDetails.profile_pic;
	  		$scope.proBusinessRating = data.proUserDetails.business.rating;
	  		$scope.requesterObj.latest_location = data.latest_location;	  		
	  		
	  		$scope.jobName = data.jobName;
	  		$scope.jobDetails = data.jobDetails;
	  		$scope.jobRef = data.jobRef;
	  		var jobObject = $firebaseObject($scope.jobRef);
	  		jobObject.$loaded(function(jobData){	  			
	  			$scope.workComplete = jobData.workComplete;
	  			$scope.jobReviewed = jobData.reviewed;
	  		})

	  		$scope.proQuote = data.proQuote;
	  		$ionicLoading.hide();
		 });

		});
		
}

	$scope.addReview = function() {
		if($scope.bidder_id) {
			$scope.jobRef.update({'reviewed':true});
			$state.go('app.review', {'id':$scope.bidder_id});
		}		
	}

	$scope.completeJob =function() {
		var SPRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.bidder_id+'/business/jobs/'+$scope.jobKey).update({'status':'complete'})
		$scope.jobRef.update({'workComplete':true});		
	}

	
	$scope.launchNavigator = function() {
	  var destination = [$scope.requesterObj.latest_location.lat, $scope.requesterObj.latest_location.lng];

	  var start = [$scope.userLat,$scope.userLng];
	 	
	  $cordovaLaunchNavigator.navigate(destination, start).then(function() {
	    console.log("Navigator launched"); 
	  }, function (err) {
		  console.error(err);
		});
 };
	
}]);


