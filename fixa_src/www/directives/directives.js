'use strict';

angular.module('MyApp.directives').directive('backButton', function(){
	return {
    restrict: 'A',
    template:'<i class="button-icon icon ion-ios-arrow-left energized"></i>',
    link: function(scope , element, $ionicNavBarDelegate){
     element.bind("click", function(e){
         $ionicNavBarDelegate.setTitle("Test");
     });
    }
  }

});