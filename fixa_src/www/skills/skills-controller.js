'use strict';

angular.module('MyApp.controllers').controller('SkillCtrl', 
    ['$scope', 'FIREBASE_ROOT', 'Firebase', '$state', '$firebaseArray', '$firebaseObject', '$stateParams',
    function($scope, FIREBASE_ROOT, Firebase, $state, $firebaseArray, $firebaseObject, $stateParams) {

	var ref = new Firebase(FIREBASE_ROOT);
	var loggedInUser = ref.getAuth();
	var loggedInUserUid  = loggedInUser.uid.substring(12);
 	var loggedInUserRef = new Firebase(FIREBASE_ROOT+"/users/"+loggedInUserUid);
  var loggedInUserObj = $firebaseObject(loggedInUserRef);
  loggedInUserObj.$loaded(function(data){
  	$scope.loggedInUserContractorStatus = data.contractorStatus;
  })

  if($stateParams) {
    var user_uid = $stateParams.id;
    var userRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
    var userObj = $firebaseObject(userRef);
    userObj.$loaded(function(data){
    		console.log(data.contractorStatus);
        $scope.contractorStatus = data.contractorStatus;


        var skillRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid+"/skills");
  			$scope.skills = $firebaseArray(skillRef);	
  			$scope.allSkills;
        if(skillRef != null || skillRef != 'undefined') {
            $scope.allSkills = $firebaseArray(skillRef);
        } 
    });     
  }

	$scope.addskill = function (text) {  
	    if(skillRef != null || skillRef != 'undefined') {  
	      $scope.skills.$add(text);
	    }
	    //$scope.allSkills.push(text);
	    //$scope.skillText = '';
	};

	$scope.clearCompleted = function () {
	    $scope.todos = _.filter($scope.todos, function(todo){
	        return !todo.done;
	    });
	};

	$scope.onItemDelete = function(id) {
	    var index = $scope.skills.$getRecord(id);
	    $scope.skills.$remove(index);
	};

	$scope.save = function() {
	    $state.go('app.profile');
	};

}]);