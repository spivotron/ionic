'use strict';

angular.module('MyApp.controllers').controller('ManageBizCtrl', ['$scope','Auth', '$firebase', '$firebaseAuth', 'FIREBASE_ROOT',
	function ($scope, Auth, $firebase, $firebaseAuth, FIREBASE_ROOT) {

	var ref = new Firebase(FIREBASE_ROOT+'/users');
	var user = ref.getAuth();
    $scope.user = user;
    var user_uid= user.uid.substring(12);
	var bizRef  = new Firebase(FIREBASE_ROOT+"/users/"+user_uid+"/business");
	var userRef = new Firebase(FIREBASE_ROOT+"/users/"+ user_uid);

	$scope.edit_off = 1;
	$scope.edit = function()
	{
		$scope.edit_on = 1;
		$scope.edit_off = 0;
	};
	$scope.save = function()
	{
	    if (!$scope.biz_stuff) {
	        userRef.child('business').update($scope.biz_stuff);
	   	};

		userRef.child('business').update({name:$scope.biz_stuff.name, category:$scope.biz_stuff.category,
			rate:$scope.biz_stuff.rate, address: $scope.biz_stuff.address, phone:$scope.biz_stuff.phone, 
			website: $scope.biz_stuff.website, about:$scope.biz_stuff.about});
		$scope.edit_on = 0;
		$scope.edit_off = 1;
	}

    bizRef.on("value", function (snapshot) {
      var biz = snapshot.val();
      $scope.biz_stuff = biz;
    });

}]);
