'use strict';

angular.module("MyApp.controllers").controller('LoginCtrl',
  function ($scope, $state, $ionicLoading, Auth, User, FIREBASE_ROOT, userSession,$cordovaDialogs, Firebase, $ionicModal) {

    var ref = new Firebase(FIREBASE_ROOT+'/users/');
    var user = ref.getAuth();
    var FBDBref= new Firebase(FIREBASE_ROOT+'/FBusersDB/');
    var root_ref = new Firebase(FIREBASE_ROOT);

    $scope.user = {
      email: '',
      password: ''
    };
    $scope.newemail='';

    $ionicModal.fromTemplateUrl('./login/my-modal.html', {
       scope: $scope,
       animation: 'slide-in-up'
     }).then(function(modal) {
       $scope.modal = modal;
     });
     $scope.openModal = function() {
       $scope.modal.show();
     };
     $scope.closeModal = function() {
       $scope.user.email=$scope.newemail;
       var fb_uid = $scope.fb_uid;
       $scope.FBuserObj[fb_uid].email = $scope.fb_email.fb_email;
       $scope.FBuserObj[fb_uid].name = $scope.fb_name.fb_name;
       $scope.FBuserObj[fb_uid].phonenumber=$scope.fb_phonenumber.fb_phonenumber;
       $scope.FBuserObj[fb_uid].zip = $scope.fb_zip.fb_zip;
        
       if ($scope.fb_contractorstatus.fb_contractorstatus ==="true"){
        $scope.FBuserObj[fb_uid].contractorStatus = true; 
       } else if ($scope.fb_contractorstatus.fb_contractorstatus ==="false") {
        $scope.FBuserObj[fb_uid].contractorStatus = false; 
       }
       root_ref.child('FBusersDB').update($scope.FBuserObj , function(error) {
         if (error) {
           console.log("Error inserting user: ", error);
         }
         $scope.modal.hide();
         goToDash();
       });

       
    };
      

     //Cleanup the modal when we're done with it!
     $scope.$on('$destroy', function() {
       $scope.modal.remove();
     });
     // Execute action on hide modal
     $scope.$on('modal.hidden', function() {
       // Execute action
     });
     // Execute action on remove modal
     $scope.$on('modal.removed', function() {
       // Execute action
     });

    $scope.errorMessage = null;
    $scope.login = function() {
      $scope.errorMessage = null;
      $ionicLoading.show({
        template: 'Please wait...'
      });
      return Auth.login($scope.user.email, $scope.user.password);
      goToDash();
        //.catch(handleError);
    };

    
    $scope.loginWithFB = function() {
      Auth.loginWithFacebook()
      .then(function(authData){
        storeUser(authData);
        
      })
      .catch(function(error) {
        console.error(error);
      });
    }

    function storeUser(fb_user){
      //pulls data from the github user data to create a cleaner
      //filtered user object that we insert to the database
      
      // contractorStatus:false, phonenumber:phonenumber, zip:zip
      var fb_uid = fb_user.uid;
      $scope.fb_uid = fb_user.uid;

      $scope.fbEmailDup = $scope.fbEmail;
      var filteredUser = {
        'fb_uid' : fb_uid
        };
      $scope.FBuserObj = {};
      $scope.FBuserObj[fb_uid] = filteredUser;

      $scope.fb_email = {fb_email: fb_user.facebook.email};
      $scope.fb_name = {fb_name:fb_user.facebook.displayName};
      $scope.fb_phonenumber={fb_phonenumber: null};
      $scope.fb_zip = {fb_zip: null};
      $scope.fb_contractorstatus = {fb_contractorstatus: false};
        
      root_ref.child('FBusersDB').on("value", function(snapshot) {
        var FBusers = snapshot.val();
        //check to see if user exists already
        //if not then add them to the db
        if(!FBusers[fb_uid]) {
          $scope.openModal();
          
        } else {
          goToDash();
        }
      }, function (error) {
        console.log("The read failed: " + error);
      });
      
    };


    function goToDash()
    {
      console.log("going to dash");
      $ionicLoading.hide();
   
      if(user===null) {
        // this means user is signed in from FB currently. need to implement more logic
        $state.go("app.dashboard");
      }
      else if (user.contractorStatus) {
        $state.go("app.schedule")
      }else {
        $state.go("app.dashboard");
      }
      
    };

    $scope.logout = function() {
      auth.$logout($scope.user.email, $scope.user.password);
      $state.go('login');
    }

    function redirectBasedOnStatus() {

      $ionicLoading.hide();

      if (User.hasChangedPassword()) {
        $state.go('app.dashboard');
      } else {
        $state.go('change-password');
      }

    }

    function handleError(error) {
      switch (error.code) {
        case 'INVALID_EMAIL':
        case 'INVALID_PASSWORD':
        case 'INVALID_USER':
          $scope.errorMessage = 'Email or password is incorrect';
          console.log($scope.errorMessage)

          break;
        default:
          $scope.errorMessage = 'Error: [' + error.code + ']';
          console.log($scope.errorMessage)
      }

      $ionicLoading.hide();
    }

});
