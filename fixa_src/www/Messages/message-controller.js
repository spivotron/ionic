"use strict";
angular.module("MyApp.controllers").controller("MessageCTRL", 
  ['$scope', '$firebaseObject', '$stateParams','FIREBASE_ROOT', 'numOfMessages', 'CloudinaryService',  '$cordovaCamera','$cordovaGeolocation','$cordovaFileTransfer', 
  function($scope, $firebaseObject, $stateParams, FIREBASE_ROOT, numOfMessages, CloudinaryService , $cordovaCamera, $cordovaGeolocation, $cordovaFileTransfer){
    var ref = new Firebase(FIREBASE_ROOT+'/users/');
    var user = ref.getAuth();
    var root = new Firebase(FIREBASE_ROOT);
    
    var chat_root = $stateParams.rootId;
    var roomId = $stateParams.roomId;
    $scope.roomId = roomId; 
    var chat_ref = new Firebase(FIREBASE_ROOT+'/chat/'+chat_root+'/');
    $scope.chat_ref = chat_ref;
    var chat = new Firechat(chat_ref);
    $scope.user_fullId = user.uid;
    var user_uid = user.auth.uid.substring(12);
    $scope.messages = [];
    var msg_ref = new Firebase(FIREBASE_ROOT+'/chat/'+chat_root+'/room-messages/'+roomId);
    var msg_ref_obj = $firebaseObject(msg_ref);
    $scope.newMessage=null;
    var userRef= new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
    var userRefObj = $firebaseObject(userRef);
    var user2_uid = $stateParams.rootId.match(/-simplelogin:\d+/)['input'].slice(-24);
    var user2Ref = new Firebase(FIREBASE_ROOT+"/users/"+user2_uid);
    var user2RefObj = $firebaseObject(user2Ref);
    userRefObj.$loaded(
      function(data) {
        $scope.contractorStatus  = data.contractorStatus;
        $scope.profile_pic = data.profile_pic;

        $scope.profile_email = data.email;
        $scope.profile_pic = data.profile_pic;
        
        $scope.user_name = data.name;
        
        $scope.user.first_name = data.first_name;
        $scope.user.last_name  = data.last_name;

        $scope.user.phone_number = data.phone_number;
      }
    ).then(function(){
    	user2RefObj.$loaded(
      function(data) {
        $scope.profile_pic2 = data.profile_pic;
        console.log(data);
      }
    )
    })

    
    
    
    chat.setUser(user.uid, user.uid, function(chat_user) {
      chat.enterRoom(roomId);

    });

    $scope.sendNewMessage=function(newMessage){

      chat.sendMessage(roomId, newMessage, 'default', '', function(result){
        newMessage ='';
        $scope.newMessage ='';
      })
    
    };
    $scope.offset = 10; 

    $scope.pullToRefresh = function(){
      if ($scope.offset < numOfMessages) {
        $scope.offset =  $scope.offset +10;
        var query2 = $scope.chat_ref.child('room-messages').child($scope.roomId).orderByChild("timestamp").limitToLast($scope.offset);
        query2.once('value', function(snapshot){
          var message = snapshot.val();
          $scope.messages =[];
          for (var key in message) {
            $scope.messages.push(message[key]);
          }
          $scope.$apply($scope.messages);
        })
      }
      else {
        $scope.offset = numOfMessages;
      }
      
    }
    var query = chat_ref.child('room-messages').child(roomId).orderByChild("timestamp").limitToLast(10);
   
    query.on('child_added', function(snapshot) {
      var message = snapshot.val();
      $scope.messages.push(message);
    });  

    $scope.checkId= function(id){
      return id === $scope.user_fullId;
    }

    $scope.takePicture = function() {
      var options = {
          quality : 75,
          destinationType : Camera.DestinationType.DATA_URL,
          sourceType : Camera.PictureSourceType.CAMERA,
          allowEdit : true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 300,
          targetHeight: 300,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
          $scope.imageURI = imageData;
          
          return CloudinaryService.uploadImage($scope.imageURI);
      })
      .then(function(result){
          var url = result.secure_url || '';
          var urlSmall;

          if(result && result.eager[0]) {
            urlSmall = result.eager[0].secure_url || '';
            console.log('url ~~~~~~~~ is ', urlSmall);
            chat.sendMessage(roomId,'', 'default', urlSmall, function(result){
              console.log('url is ', urlSmall);
              console.log('message image url successfully updated to firebase');
            })
          }

          // Do something with the results here.

          $cordovaCamera.cleanup();
      }, function(err){
          // Do something with the error here
          $cordovaCamera.cleanup();
      });
    };

    


}])