"use strict";
angular.module("MyApp.controllers").controller("MessagesCTRL", 
  ['$scope', '$firebaseObject', '$state','FIREBASE_ROOT', 
  function($scope, $firebaseObject, $state, FIREBASE_ROOT){

    var ref = new Firebase(FIREBASE_ROOT+'/users/');
  	var user = ref.getAuth();
    var root = new Firebase(FIREBASE_ROOT);

    var user_uid = user.auth.uid.substring(12);
    var msg_ref = new Firebase(FIREBASE_ROOT+'/users/'+user_uid+'/messageChannels');
    var msg_ref_obj = $firebaseObject(msg_ref);
    var users_ref_obj = $firebaseObject(ref);
    $scope.messages = [];

    $scope.goToMessage = function(rootId, roomId){
      $state.go('app.message', {rootId: rootId, roomId: roomId});
    }

    msg_ref_obj.$loaded().then(function(data){ 
      data.forEach(function(item){
        $scope.messages.push(item);
      })
      return users_ref_obj.$loaded();
    })
    .then(function(data){
      data.forEach(function(user){
        for (var i=0; i< $scope.messages.length; i++) {
          var message_root = $scope.messages[i].root;
          if (user.messageChannels!==undefined && user.messageChannels[message_root] !==undefined){            
            $scope.messages[i].profile_pic = user.profile_pic;
            $scope.messages[i].name = user.business.name;
          }
        }
      })
    })
    

    
    
	
}])