'use strict';

angular.module('Auth', [])
.service('Auth', function ($q, $rootScope, $firebase, 
  $firebaseAuth, $state, $ionicLoading, $timeout, $cordovaDialogs,$localStorage, FIREBASE_ROOT) {
    
    var self = this;
    var fref = new Firebase(FIREBASE_ROOT);
    var ref = $firebaseAuth(fref)
    this.currentUser = ref;
    this.user = {profile:{} };
    // user specific reference
    var profile_id;
   
    this.getCurrentUser = function() {
      var defer = $q.defer();
      
      firebaseSimpleLogin.$getCurrentUser().then(function(user) {
        self.currentUser = user;
        
        if (user === null) {
          defer.reject();
        } else {
          defer.resolve(user);
        }
      });

      return defer.promise;
    };

    this.createUser = function(email, password, name, phonenumber, zip) {
      $ionicLoading.show({
        template: '<i class="ion-loading-c"></i><br /><h4>Please wait...</h4>',
        noBackdrop: false
      });
      ref.$createUser({email: email, password: password}).then(function() {
            return ref.$authWithPassword({
                email: email,
                password: password,
                name:name,
                phonenumber:phonenumber,
                zip:zip
            });
        }).then(function(authData) {
            
            fref.child('users').child(authData.uid.substring(12)).set({email:email, contractorStatus:false, name:name, phonenumber:phonenumber, zip:zip});
            
            $timeout(function(){
                $ionicLoading.hide();
                $state.go("app.dashboard");
            },2000);
        }).catch(function(error) {
          var _error = error.toString();
          $cordovaDialogs.alert(_error, 'Oops!', 'OK');
          $ionicLoading.hide();
        });
    }

    this.login = function(email, password) {
      
      fref.authWithPassword({
        email    : email,
        password : password
      }, function(error, authData) {
        if (error) {
          $ionicLoading.hide();
          switch (error.code) {
            case "INVALID_EMAIL":
              $cordovaDialogs.alert("The specified user account email is invalid.", 'Oops! Invalid Email!', 'OK');
              break;
            case "INVALID_PASSWORD":
              $cordovaDialogs.alert("The specified user account password is incorrect.", 'Oops! Invalid Password!', 'OK');
              break;
            case "INVALID_USER":
              $cordovaDialogs.alert("The specified user account does not exist.", 'Oops! User does not exist!', 'OK');
              break;
            default:
              var _error  = error.toString();
              $cordovaDialogs.alert("The specified user account does not exist.", 'Oops! Error, '+_error, 'OK');
             
          }
        } else {
  
          $ionicLoading.hide();
          $state.go("app.dashboard")
          
        }
      });
    }
    
    this.logout = function() {
      ref.$unauth();
      this.currentUser = null;
    };

    this.onAuth = function(callback) {
      ref.$onAuth(function(authData) {
        $timeout(function() {
          callback(authData);
        });
      });
    };

    this.loginWithProvider = function(provider) {
       return ref.$authWithOAuthPopup("facebook", {remember: "sessionOnly",scope: "email,user_friends"});
    };

    this.loginWithFacebook = function() {
      return this.loginWithProvider("facebook");
    };

    // no longer working with simple login so but with Firebase API
    // this.sendPasswordResetEmail = function(email) {
    //   return firebaseSimpleLogin.$sendPasswordResetEmail(email);
    // }; 

    this.changePassword = function(email, oldPassword, newPassword) {
      // return firebaseSimpleLogin.$changePassword(this.currentUser.email, oldPassword, newPassword);
      $ionicLoading.show({
              template: '<i class="ion-loading-c"></i><br /><h4>Please wait...</h4>',
              noBackdrop: false
            });
      fref.changePassword({
        email: email,
        oldPassword: oldPassword,
        newPassword: newPassword
      }, function(error) {
        if (error) {
          switch (error.code) {
            case "INVALID_PASSWORD":
              console.log("The specified user account password is incorrect.");
              break;
            case "INVALID_USER":
              console.log("The specified user account does not exist.");
              break;
            default:
              console.log("Error changing password:", error);
          }
        } else {
          console.log("User password changed successfully!");
          $ionicLoading.hide();
          $ionicLoading.show({
                template: '<i class="ion-checkmark"></i><br /><h4>User password changed successfully!</h4>',
                noBackdrop: false
              });
          $timeout(function(){
              $ionicLoading.hide();
          },2000);
        }
      });
    };

    
    function handleError(error) {
      switch (error.code) {
        case 'INVALID_EMAIL':
          $scope.errorMessage = 'Invalid email';
          break;
        case 'EMAIL_TAKEN':
          $scope.errorMessage = 'Email already exists';
          break;
        default:
          $scope.errorMessage = 'Error: [' + error.code + ']';
      }
    };
    
});