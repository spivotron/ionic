'use strict';

angular.module('MyApp.controllers').controller('DashboardCtrl', ['$scope','Auth', 'User',
  '$rootScope', '$ionicLoading', '$ionicPopup', '$ionicModal', '$cordovaGeolocation',
  '$cordovaKeyboard', '$firebase', '$firebaseAuth', '$ionicUser', '$cordovaPush',
  '$cordovaLocalNotification', '$firebaseArray', 'FIREBASE_ROOT', '$firebaseObject', '$interval',
  '$http', '$state','$timeout', '$ionicPlatform', '$cordovaDialogs',
  function ($scope, Auth, User, $rootScope, $ionicLoading, $ionicPopup, $ionicModal, $cordovaGeolocation,
    $cordovaKeyboard, $firebase, $firebaseAuth, $ionicUser, $cordovaPush,
    $cordovaLocalNotification, $firebaseArray, FIREBASE_ROOT, $firebaseObject,
    $interval, $http,$state, $timeout, $ionicPlatform, $cordovaDialogs) {


  // declare necessary variables
  var ref = new Firebase(FIREBASE_ROOT+'/users/');
  var user = ref.getAuth();
  $scope.devToken;
  $scope.dashTitle ="FIXA";
  
  //Should be called once the device is registered successfully with Apple or Google servers
 
  // get platform specific details
  var isIOS = ionic.Platform.isIOS();
  var isAndroid = ionic.Platform.isAndroid();

  $ionicModal.fromTemplateUrl('add_business/business-list-modal.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.modal = modal;
   });

  $scope.openModal = function() {
       $scope.modal.show();
     };

  $scope.init = function()
  {
    locate_user();
  };
  $scope.init();

  // // PushBots init. Comment these lines when testing browser
  // window.plugins.PushbotsPlugin.initialize("56bb81481779592c4b8b4567");

  //  //Should be called once app receive the notification
  // window.plugins.PushbotsPlugin.on("notification:received", function(data){
  // 	var popup;    
  //     userRefObj.$loaded(function(u_data){ // load specific user data              
  //      if (u_data.contractorStatus) {
		// 			popup = $ionicPopup.alert({
		// 				title: 'New Request in your area!',
		// 				okType: "button-energized no-border",
		// 				okText: "View Request"
		// 			}); 
		// 			popup.then(function(){
		// 				$state.go('app.'+data.contractorState, {uid:data.uid, date:data.date, time:data.time, id:data.bidder_id, key: data.key});
		// 			});      	          	
  //      } else {
		// 			popup = $ionicPopup.alert({
		// 				title: 'New Quote!',
		// 				okType: "button-energized no-border",
		// 				okText: "View Quote"
		// 			});
		// 			popup.then(function(){
		// 				$state.go('app.'+data.userState, { uid: $scope.user.uid.substring(12), date: data.date, time: data.time, id:data.bidder_id, key: data.key});
		// 			});         
  //      }
  //     });
  //   });
  //  // Should be called once the notification is clicked
  //  // **important** Doesn't work with iOS while app is closed
  
  // window.plugins.PushbotsPlugin.on("notification:clicked", function(data){     
  //    userRefObj.$loaded(function(u_data){ // load specific user data              
  //      if (u_data.contractorStatus) {
  //        $state.go('app.'+data.contractorState, {uid:data.uid, date:data.date, time:data.time, id:data.bidder_id, key:data.key});
  //      } else {
  //        $state.go('app.'+data.userState, { uid: $scope.user.uid.substring(12), date: data.date, time: data.time, id:data.bidder_id, key:data.key});
  //      }
  //    });
  //  });
    
  // if (user)
  // {
  //  var user_uid = user.auth.uid.substring(12);
  //  var userRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
  //  // send token to firebase
  //  window.plugins.PushbotsPlugin.on("registered", function(token){
      
  //    if (isIOS) {
  //      userRef.update({"device":{"deviceToken":token, "platform":0}});
  //    } else if (isAndroid) {
  //      userRef.update({"deviceToken":token, "platform":1});
  //    }
  //  }); // end on "registered"

  //  $scope.user = user; 
  //  var userObj = new Firebase(FIREBASE_ROOT + '/users/' + $scope.user.uid.substring(12));
  //  var userRefObj = $firebaseObject(userObj);
  // };

  function locate_user(){
    $cordovaGeolocation
      .getCurrentPosition()
      .then(function (position) {
        $scope.lat  = position.coords.latitude;
        $scope.lng = position.coords.longitude;
      }, function(err) {
        // error
        console.log("Location error!");
        console.log(err);
    });
  };

  $scope.goto = function(state){
    $state.go("app.list",{"category":state})
  }

  // test for undefined user before sending them to new state


   $scope.test = function(goto) {
    if (!user) {
      $cordovaDialogs.confirm('Create an account or login to send and receive requests for service.  It\'s all part of our commitment to helping you find the best pro for the job.', 
        'For access please sign up', ['Cancel', 'Login']).then(function(buttonIndex){
          if (buttonIndex == 2) {
            $state.go('login');
          };
        })
    } else {
      $state.go("app."+goto.toString());
    }
  }

  $scope.categorySelections = [
      {name: 'Choose a category', value:""},
      {name: 'Cleaners', value:"Cleaners", icon:"cleaners", id:"cleaner", title:"Cleaner"},
      {name: 'General Contractors', value:"General Contractors", icon:"contractors", id:"contractor", title:"Contractor"},
      {name: 'Electricians', value:"Electricians", icon:"electrician", id:"electrician", title:"Electrician"},
      {name: 'Flooring Installers', value:"Flooring Installers", icon:"flooring", id:"flooring", title:"Flooring"},
      {name: 'Handymen', value:"Handymen", icon:"handyman", id:"handyman", title:"Handyman"},
      {name: 'HVAC Technicians', value:"HVAC Technicians", icon:"hvac", id:"hvac", title:"HVAC"},
      {name: 'Landscapers', value:"Landscapers", icon:"landscaper", id:"landscaper", title:"Landscaper"},
      {name: 'Locksmiths', value:"Locksmiths", icon:"locksmith", id:"locksmith", title:"Locksmith"},
      {name: 'Movers', value:"Movers", icon:"movers", id:"movers", title:"Movers"},
      {name: 'Painters', value:"Painters", icon:"painter", id:"painters", title:"Painters"},
      {name: 'Plumbers', value:"Plumbers", icon:"plumbers", id:"plumber", title:"Plumbers"},
      {name: 'Pool Cleaners', value:"Pool Cleaners", icon:"pool-cleaners", id:"pool-cleaner", title:"Pool Cleaner"},
      {name: 'Roofers', value:"Roofers", icon:"roofers", id:"roofer", title: "Roofing"}
  ];

  
    
}]);