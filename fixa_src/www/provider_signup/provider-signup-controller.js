'use strict';

angular.module('MyApp.controllers').controller("ProviderSignupCtrl", ['$scope', '$q', '$state', '$ionicLoading', 
  '$rootScope', 'User', 'Signup', 'ProviderAuthService','Auth', '$cordovaDialogs', '$ionicPopup',
    function ($scope, $q, $state, $ionicLoading, $rootScope, User, Signup, ProviderAuthService, Auth, $cordovaDialogs, $ionicPopup) {
  
    $scope.user = {
      first_name: '',
      last_name: '',
      password: '',
      email: '',
      city: '',
      state: '',
      license_number:'',
      contractorStatus: true
    };
    $scope.errorMessage = null;

    $scope.signup = function() {
      $scope.errorMessage = null;
      
      if (checkPassword($scope.user.password, $scope.user.password2) == 0)
      {
          $state.go('provider_signup');
          $ionicLoading.hide();
      }
      else
      { 
        createAuthUser()
        // on success, goes to add_business
      }
      
    };

    function createAuthUser() {
      // use the variables here to register sp on PushBots
      return ProviderAuthService.createUser($scope.user.email, $scope.user.password, $scope.user.first_name, 
        $scope.user.last_name, $scope.user.phone, $scope.user.license_number, $scope.user.contractorStatus
      );
    }

    function checkPassword(password1, password2)
    { 
        if (password1 != password2)
        {
          $cordovaDialogs.alert("Your passwords didn't match!", 'Password mismatch', 'OK');
          return 0;
        }
    };
    function goToAddBiz() {
      $ionicLoading.hide();
      console.log("going to add_business")
      $state.go('add_business');
    }

    function sendPasswordResetEmail(authUser) {
      var defer = $q.defer();

      Auth.sendPasswordResetEmail(authUser.email).then(function() {
        defer.resolve(authUser);
      });

      return defer.promise;
    };

    function login(authUser) {
      return Auth.login($scope.user.email, $scope.user.password);
    };

    function createMyAppUser(authUser) {
      return User.create_service_provider(authUser.uid, authUser.email, $scope.user.first_name, 
        $scope.user.last_name, $scope.user.phone, 
        $scope.user.city, $scope.user.state, $scope.user.license_number,$scope.user.contractorStatus);
    };

    function goToChangePassword() {
      $ionicLoading.hide();
      $state.go('change-password');
    };

    function handleError(error) {
      switch (error.code) {
        case 'INVALID_EMAIL':
          $scope.errorMessage = 'Invalid email';
          break;
        case 'EMAIL_TAKEN':
          $scope.errorMessage = 'Email already exists';
          break;
        default:
          $scope.errorMessage = 'Error: [' + error.code + ']';
      }

      $ionicLoading.hide();
    };
    

}]);