'use strict';


angular.module('ProviderAuthService',[])
.service("ProviderAuthService", function ($q, Auth, FIREBASE_ROOT, $rootScope, $firebase, $firebaseAuth, 
	$ionicLoading, $timeout, $state, $cordovaDialogs) {

	var self = this;
	this.currentUser = null;
	var fref = new Firebase('https://bucket-list-app.firebaseio.com/');
	var ref = $firebaseAuth(fref)
	
	this.checkLicense= function(license_number)
    {
    	var license_number_url = "https://www2.cslb.ca.gov/onlineservices/checklicenseII/LicenseDetail.aspx?LicNum=";

        var license_number_url = license_number_url+license_number;
        //var status = license_number_url.getElementById("ctl00_LeftColumnMiddle_Status");
        console.log(license_number_url);
        //console.log(status);
       
    };
    function checkAcct(phone, license_number)
    {
    	if(!phone || !license_number)
    	{
    		$cordovaDialogs.alert("All fields are required.", 'Oops!', 'OK');
    		$ionicLoading.hide();	
    		return 0;
    	}
       	else 
    	{
    		return 1;
    	}
    	
    }
	this.createUser = function(email, password,first_name,last_name, phone, city, state, license_number, serviceProvider) {
	  $ionicLoading.show({
	    template: '<i class="ion-loading-c"></i><br /><h4>Please wait...</h4>',
	    noBackdrop: false
	  });
	  	ref.$createUser({email: email, password: password}).then(function() {
	        return ref.$authWithPassword({
	            email: email,
	            password: password
	        });
	    }).then(function(authData) {	        
	        fref.child('users').child(authData.uid.substring(12)).set({email:email, first_name:first_name, 
	        	last_name:last_name,city:city, state:state,
	        	contractorStatus:true});
	        
	        $timeout(function(){
	            $ionicLoading.hide();
	            $state.go("add_business")
	        },2000);
	    }).catch(function(error) {
	        var _error = error.toString();
	        $cordovaDialogs.alert("Please check all fields", "Oops!");
	        $ionicLoading.hide();
	    }); 
	  
	}
});