'use strict';

var NOODLIO_PAY_API_URL         = "https://noodlio-pay.p.mashape.com"; 
var NOODLIO_PAY_API_KEY         = "2BvnEdy1gomshE8vBE5onpRFu2Kip1DxkEGjsnnkxdeAB8Vh9w";
var NOODLIO_PAY_CHECKOUT_KEY    = {test: "pk_test_OpUgI8wNJiL4DoiFzT3qCTqs", live: "pk_live_Nv2b2kG1bkxE6V17NgDS2A7R"};

var STRIPE_ACCOUNT_ID = "acct_15T28FCNsXJZlMGo";
var TEST_MODE = false;


var fixy  = angular.module('fixa', [
  'ionic',
  'firebase',
  'ngRoute',
  'stripe.checkout',
  'ngResource',
  'MyApp.services',
  'MyApp.directives',
  'MyApp.controllers',
  'MyApp.filters',
  'com.socialAuth.services',
  'ngCordova',
  'LocationService',
  'Auth',
  'PhotoService',
  'base64',
  'SearchFactory',
  'ProviderAuthService',
  'ListView',
  'ionic.service.core',
  'ionic.service.push',
  'ngStorage',
  'SendService',
  'ksSwiper',
  'ionic.rating',
  'CloudinaryService',
  'StripeCharge',
  'RequestData',
  'QuoteData',
  'ionic-modal-select'
])

.config(['$stateProvider', '$urlRouterProvider', 'StripeCheckoutProvider', 
	function($stateProvider, $urlRouterProvider, StripeCheckoutProvider) {

  // for payment processing
  switch (TEST_MODE) {
    case true:
      //
      StripeCheckoutProvider.defaults({key: NOODLIO_PAY_CHECKOUT_KEY['test']});
      break
    default:
      //
      StripeCheckoutProvider.defaults({key: NOODLIO_PAY_CHECKOUT_KEY['live']});
      break
  };



  var resolve = {
    auth: function($q, $timeout, Auth, User, $state) {
      var defer = $q.defer();
      var state = this;

      if (User.loadCurrentUser()) {
            $state.go('app.dashboard');
      }else
      {
        $state.go("login")
      }

      return defer.promise;
    }
  }

  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      cache: false,
      templateUrl: 'sidemenu/sidemenu.html',
      controller: 'SideMenuCtrl'
    })
    .state('signup', {
      url: '/signup',

      templateUrl: 'signup/signup.html',
      controller: 'SignupCtrl'
    })
    .state('login', {
      url: '/login',
      templateUrl: 'login/login.html',
      controller: 'LoginCtrl'
    })
    .state('first', {
      url: '/first',
      templateUrl: 'login/first.html',
      controller: 'LoginCtrl'
    })
    .state('choose', {
      url: '/choose',
      templateUrl: 'login/choose-account-type.html',
      controller: 'LoginCtrl'
    })
    .state('app.reset-password', {
      url: '/reset-password',
      views: {
        menuContent:{
          templateUrl: 'reset-password/reset-password.html',
          controller: 'ResetPasswordCtrl'
        }
      }
      
    })
    .state('change-password', {
      url: '/change-password',
      templateUrl: 'change-password/change-password.html',
      controller: 'ChangePasswordCtrl',
      resolve: resolve
    })
    .state('provider_signup', {
      url: '/provider_signup',
      templateUrl: 'provider_signup/signup.html',
      controller: 'ProviderSignupCtrl'
    })
    .state('add_business',{
      url: '/add_business',
      templateUrl: 'add_business/add_business.html',
      controller: 'AddBizCtrl'
    })
    .state('app.dashboard', {
      url: '/dashboard',
      // It looks like we are adding in the sidemenu from the 'app' state here
      views: {
        menuContent: {
          templateUrl: 'dashboard/dashboard.html',
          controller: 'DashboardCtrl',
          //resolve: resolve
        }
      }
    }).state('app.manageBiz',{
        url:'/manage_biz',
        views: {
          menuContent: {
            templateUrl: 'manage-biz/manage.html',
            controller: 'ManageBizCtrl',
          }
        }

    }).state('app.manageAcct',{
        url:'/manage_acct',
        views: {
          menuContent: {
            templateUrl: 'manage-account/manage-account.html',
            controller: 'ManageAcctCtrl',
          }
        }

    })
    .state('app.settings',{
        url:'/settings',
        views: {
          menuContent: {
            templateUrl: 'settings/settings.html',
            controller: 'SettingsCtrl',
          }
        }

    })
    .state("app.newRequest", {
      url:"/newrequest",
      views: {
        menuContent: {
          templateUrl: "request/new-request.html",
          controller: 'NewRequestCtrl',
        }
      }
    })
    .state('app.profile',{
        url:'/profile',
        views: {
          menuContent: {
            templateUrl: 'Profile/profile.html',
            controller: 'ProfileCtrl',
            //resolve: resolve
          }
        }

    })
    .state('edit',{
        url:'/edit',
        templateUrl: 'Profile/profile_edit.html',
        controller: 'ProfileCtrl'

    })
    .state('app.quotes',{
      url:'/jobs/:uid/:name/:date/:time/:key/quotes/',
      views: {
          menuContent: {
            templateUrl: 'quotes/quotes-list.html',
            controller: 'JobHistoryCtrl',
            //resolve: resolve
          }
        }
    })
    
    .state('app.jobs',{
        url:'/jobs',
        views: {
          menuContent: {
            templateUrl: 'Profile/jobs.html',
            controller: 'JobHistoryCtrl',
            //resolve: resolve
          }
        }
    })
    .state('app.pastRequests',{
        url:'/jobs/:date/:time"',
        views: {
          menuContent: {
            templateUrl: 'Profile/pastRequests.html',
            controller: 'JobHistoryCtrl',
            //resolve: resolve
          }
        }
    })
    .state('app.messages',{
        url:'/messages',
        views: {
          menuContent: {
            templateUrl: 'Messages/messages.html',
            controller: 'MessagesCTRL'
            //resolve: resolve
          }
        }
    })
    .state('app.message',{
        url:'/message/:rootId/:roomId',
        views: {
          menuContent: {
            templateUrl: 'Messages/message.html',
            controller: 'MessageCTRL',
            resolve: {
              
              numOfMessages: function($stateParams, $q){
                console.log('$stateParams is ', $stateParams);
                var chat_root = $stateParams.rootId;
                var roomId = $stateParams.roomId;
                var chat_ref = new Firebase('https://bucket-list-app.firebaseio.com/'+'/chat/'+chat_root+'/');
                var myref = chat_ref.child('room-messages').child(roomId);
                var deferred = $q.defer();
                myref.on('value', function(snapshot){
                  deferred.resolve(snapshot.numChildren());
                });
                return deferred.promise;
              } // end numOfMessages
            }
          }
        }
    })    
    .state('app.serviceProviderJobView',{
        url:'/jobs/:uid/jobs/:date/:time/:key',
        views: {
          menuContent: {
            templateUrl: 'Profile/jobView.html',
            controller: 'ServiceProviderRequest',
            //resolve: resolve
          }
        }
    })    
    .state('app.userJobView',{
        url:'/jobs/:uid/jobs/:date/:time/quotes/:id/:key',
        views: {
          menuContent: {
            templateUrl: 'Profile/jobView.html',
            controller: 'UserRequest',
            //resolve: resolve
          }
        }
    })
    .state('app.acceptedQuote',{
      url:'/jobs/:uid/:date/:time/quotes/:id/:key',
      views: {
          menuContent: {
            templateUrl: 'quotes/accepted-quote.html',
            controller: 'QuoteCtrl',
            //resolve: resolve
          }
        }
    })
    .state('app.mostRecent',{
        url:'/jobs/mostRecent',
        views: {
          menuContent: {
            templateUrl: 'request/most-recent-job.html',
            controller: 'mostRecentCtrl',
            //resolve: resolve
          }
        }

    })
    .state('app.profileView', {
        url: '/profile/:id',
        views:
        {
            menuContent: {
                templateUrl: 'Profile/profileview.html',
                controller: 'ProfileViewCtrl',
            }
        }
    }).state('app.profileViewBusinessAbout', {
        url: '/profile/:id/business/about',
        views:
        {
            menuContent: {
                templateUrl: 'Profile/business-about.html',
                controller: 'ProfileViewCtrl',
            }
        }
    })
    .state('app.profileViewSkills', {
        url: '/profile/:id/business/skills',
        views:
        {
            menuContent: {
                templateUrl: 'skills/skills.html',
                controller: 'SkillCtrl',
            }
        }
    })
    .state('app.list', {
        url: '/list/:category',
        views:
        {
            menuContent: {
                templateUrl: 'list_view/list.html',
                controller: 'ListViewCtrl',
            }
        }
    }).state('app.search', {
      url: '/search',
      views: {
        menuContent: {
          templateUrl: 'Search/search.html',
          controller: 'SearchCtrl',
          //resolve: resolve
        }
      }
    })
    .state("app.newRequestWithCategory", {
      url:"/requestwithcategory/:category",
      views: {
        menuContent: {
          templateUrl: "request/new-request-with-params.html",
          controller: 'NewRequestCtrl',
        }
      }
    })
    .state("app.manageRequests", {
      url:"/managerequests",
      views: {
        menuContent: {
          templateUrl: "request/manage-requests.html",
          controller: 'ManageRequestsCtrl',
        }
      }
    })
    .state("walkthrough", {
      url:"/walkthrough",
      templateUrl: "walkthrough/walkthrough.html",
      controller: 'FirstTimeCtrl',
      
    })

    .state("unauthenticatedAccount", {
      url:"/unauthenticatedAccount",
      templateUrl: "walkthrough/unauthenticatedAccount.html",
      controller: 'FirstTimeCtrl',
    })
    .state("unauthenticatedMessages", {
      url:"/unauthenticatedMessages",
      templateUrl: "walkthrough/unauthenticatedMessages.html",
      controller: 'FirstTimeCtrl',
    })
    .state("unauthenticatedMyRequests", {
      url:"/unauthenticatedMyRequests",
      templateUrl: "walkthrough/unauthenticatedMyRequests.html",
      controller: 'FirstTimeCtrl',
    })
    .state("unauthenticatedRequest", {
      url:"/unauthenticatedRequest",
      templateUrl: "walkthrough/unauthenticatedRequest.html",
      controller: 'FirstTimeCtrl',
    })
    .state("app.skills", {
      url:"/skills",
      views: {
        menuContent: {
          templateUrl: "skills/skills.html",
          controller: 'SkillCtrl',
        }
      }
    })
    .state("app.review", {
      url:"/review/:id",
      views: {
        menuContent: {
          templateUrl: "review/review.html",
          controller: 'ReviewCTRL',
        }
      }
      
    })
    .state("app.buyCredits",{
      url:"/buyCredits",
      views: {
        menuContent : {
          templateUrl:"credit/buy-credits.html",
          controller:"CreditCtrl"
        }
      }
      
    })
    //$urlRouterProvider.otherwise('/walkthrough');  
    if (window.localStorage['firstTimeUse'] != 'no') {
      $urlRouterProvider.otherwise('/walkthrough');  
    }
}])

.run(function($rootScope, $state, $ionicPlatform, userSession, Auth,
  $firebase,  $cordovaPush, Firebase, User, $location,
  $cordovaDialogs, $cordovaSplashscreen, $timeout, $cordovaLaunchNavigator) {

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory
    // bar above the keyboard for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true)
    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    var iosConfig = {
      "badge": true,
      "sound": true,
      "alert": true,
    };
              
    // Pushbots.getRegistrationId(function(token){
    //     console.log("Registration Id:" + token);
    // });
    var fire_ref = new Firebase('https://bucket-list-app.firebaseio.com/');
    var chat = new Firechat(fire_ref);

    $timeout(function(){
      $cordovaSplashscreen.hide()
    }, 2000)

    var ref = new Firebase('https://bucket-list-app.firebaseio.com/');
    $rootScope.$on('$stateChangeError', function(event, toState, toParams,
                                                 fromState, fromParams, error) {
      $state.go(error);
    });

    var user_ref = new Firebase('https://bucket-list-app.firebaseio.com/users/');
    var user = user_ref.getAuth();
    // if this user is already signed in
    if(user) {
      $state.go('app.dashboard');
    } else {
      $state.go('walkthrough');
    }

  });
})
.constant('FIREBASE_ROOT', 'https://bucket-list-app.firebaseio.com/');

angular.module('MyApp.services', []);
angular.module('MyApp.directives', []);
angular.module('MyApp.controllers', []);
angular.module('MyApp.filters', []);
