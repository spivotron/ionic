'use strict';

angular.module('MyApp.controllers').controller('ListViewCtrl',
['$firebaseAuth', '$scope','$firebaseArray','$ionicLoading', 'FIREBASE_ROOT', '$stateParams','$state',
    function($firebaseAuth, $scope,$firebaseArray,$ionicLoading, FIREBASE_ROOT, $stateParams, $state){

    var userRef = new Firebase(FIREBASE_ROOT+'/users'); 
    $scope.category = $stateParams.category;
    $scope.listOfBusinesses = [];

    $scope.init = function()
    {
        $ionicLoading.show({
            template: 'Finding service providers in your area...'
        });

        $scope.users = $firebaseArray(userRef);
              
        $scope.users.$loaded()
          .then(function() {
            
            $ionicLoading.hide();

          })
          .catch(function(error) {
            console.log("Error:", error);
          });

    };
    $scope.init();

    

    $scope.request = function(category)
    {
      $state.go('app.newRequestWithCategory',{"category":category});
    }


}]);
