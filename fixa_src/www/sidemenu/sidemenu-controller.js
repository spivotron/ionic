'use strict';

angular.module('MyApp.controllers').controller('SideMenuCtrl', 
  function($scope, $state, Auth, FIREBASE_ROOT, $ionicPopup, $firebaseObject, $cordovaDialogs, $cordovaEmailComposer) {
    $scope.logout = function() {
      Auth.logout();
      $state.go('login');
    };

    var ref = new Firebase(FIREBASE_ROOT+'/users/');
  	var user = ref.getAuth();
  	$scope.user = user;

    if($scope.user) {
      var userObj = new Firebase(FIREBASE_ROOT + '/users/' + $scope.user.uid.substring(12));
      $scope.userObj = $firebaseObject(userObj);

      // $scope.userObj = {
      //   contractorStatus: ""
      // }
      $scope.userObj.$loaded(function(data){
        $scope.userObj.contractorStatus = data.contractorStatus;
      })
    }

    $scope.help = function() {
      var email = {
          to: 'hello@fixa.co',
          subject: 'Assistance Required with FIXA',          
          isHtml: true
        };

       $cordovaEmailComposer.open(email).then(null, function () {
         // user cancelled email
       });
    }
    
  	
  //   $scope.test = function(goto) {
  //   if (!user) {
  //     $cordovaDialogs.alert('Create an account or login to send and receive requests for service.  It\'s all part of our commitment to helping you find the best pro for the job.', 
  //       'For access please sign up', 'Ok');
  //   } else {
  //     $state.go("app."+goto.toString());
  //   }
  // }

  $scope.test = function(goto) {
    if (!user) {
      $cordovaDialogs.confirm('Create an account or login to send and receive requests for service.  It\'s all part of our commitment to helping you find the best pro for the job.', 
        'For access please sign up', ['Cancel', 'Login']).then(function(buttonIndex){
          if (buttonIndex == 2) {
            $state.go('login');
          };
        })
    } else {
      $state.go("app."+goto.toString());
    }
  }

});
