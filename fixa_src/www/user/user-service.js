'use strict';

angular.module('MyApp.services').service('User',
  function($q, $firebase, FIREBASE_ROOT, Auth, Firebase) {
    var usersRef = new Firebase(FIREBASE_ROOT + '/users');
    var currentUser = null;

    this.loadCurrentUser = function() {
      var defer = $q.defer();
      var currentUserRef = usersRef.child(Auth.currentUser.uid);
      
      currentUser = $firebase(currentUserRef);
      currentUser.$on('loaded', defer.resolve);

      return defer.promise;
    };
   
    this.isContractor = function()
    {
      var defer = $q.defer();
      var currentUserRef = usersRef.child(Auth.currentUser.uid);
      
      currentUser = $firebase(currentUserRef);
      currentUser.$on('loaded', defer.resolve);
      return currentUser.contractorStatus;
    };
    
    this.create_service_provider = function(id, email, first_name, last_name, phone,  city, state, license_number, contractorStatus) {

      var users = $firebase(usersRef);
      var new_id = id.split(':')[1];
      // need this to bind a more workable version of their id to a user's instance
      return users.child('id').set({id:new_id, email: email, first_name: first_name, 
        last_name:last_name, phone: phone,
        city:city, state:state, license_number: license_number, contractorStatus:contractorStatus });
    };

    this.create = function(id, email, contractorStatus) {

      var users = $firebase(usersRef);
      var new_id = id.split(':')[1];
      // need this to bind a more workable version of their id to a user's instance
      return users.child('id').set({id:new_id, email: email, contractorStatus:contractorStatus });
    };

    this.recordPasswordChange = function() {
      var now = Math.floor(Date.now() / 1000);
      
      return currentUser.$update({ passwordLastChangedAt: now });
    };

    this.hasChangedPassword = function() {
      return angular.isDefined(currentUser.passwordLastChangedAt);
    };
  });
