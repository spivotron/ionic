"use strict";

angular.module('MyApp.controllers').controller('mostRecentCtrl', ['WatchService', '$scope', function (WatchService, $scope){

	var mostRecentJob = WatchService.listMostRecentJob();

	$scope.job_date = mostRecentJob.date;
	$scope.job_time = mostRecentJob.time;
	$scope.job_name = mostRecentJob.name;



}])