'use strict';
angular.module("MyApp.controllers").controller("ManageRequestsCtrl", ['$scope', '$http', 'Auth','FIREBASE_ROOT',
	'$firebaseArray', '$firebaseAuth',
	function ($scope, $http, Auth, FIREBASE_ROOT, $firebaseObject, $firebaseAuth){

	var ref = new Firebase(FIREBASE_ROOT+'/users');
	var user = ref.getAuth();
    $scope.user = user;
    var user_uid  = user.uid.substring(12);
	var jobRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid+"/jobs");
	$scope.jobs = $firebaseObject(jobRef);



}]);
