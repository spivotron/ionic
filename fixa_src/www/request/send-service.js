'use strict';

angular.module("SendService", []).factory("SendService",
	['Firebase', 'FIREBASE_ROOT', '$cordovaLocalNotification', '$firebaseObject', '$q', '$http',
	function(Firebase, FIREBASE_ROOT, $cordovaLocalNotification, $firebaseObject, $q, $http){

	var ref = new Firebase(FIREBASE_ROOT + '/users/');
	var user = ref.getAuth();
	if (user) {
		var user_uid = user.auth.uid.substring(12);
		var contractorStatus;
		ref = new Firebase(FIREBASE_ROOT + '/users/' + user_uid);
		var refObj = $firebaseObject(ref);
		refObj.$loaded(function(data){
			contractorStatus = data.contractorStatus;
		})
	};
	
	

	this.send = function(user_uid, date, time, msg, deviceTokens, contractorState, userState, key)
	{
		
		// this where the POST request to PushBots is made
		// when the sp clicks on it, it will open the to the jobViewBeforeAccept view
		// iterate over device tokens and check platform type to send push accordingly

		for(var i = 0; i<deviceTokens.length; i++){

			if(deviceTokens[i].platform == 0 && deviceTokens[i]) { // check if iOS
			$http({
		      url: "https://api.pushbots.com/push/one",
		      method: "POST",
		      data: {"platform": 0,
		             "token": deviceTokens[i].deviceToken,
		             "msg":msg,
		             "sound":"appbeep.wav",
		             "payload": {
		             		"uid":user_uid,
		             		"date":date,
		             		"time":time,
		             		"contractorState": contractorState,
		             		"userState": userState,
		             		"key": key
		             	}
		            },
		      headers: {
		      	"x-pushbots-appid": "56bb81481779592c4b8b4567",
		      	"x-pushbots-secret":"db07cb4936065b1ac75bde2fe0418156",
		        "Content-Type": "application/json"
		        }
		      }).success(function (data, status, headers, config) {
		          console.log("Success", data, status);
		      }).error(function (data, status, headers, config) {
		          console.log('error installing device on pushbots...', config);
		    });
			}
			
		}

	}


	this.sendSinglePush = function(user_uid, date, time, msg, deviceToken, devicePlatform, contractorState, userState, bidder_id)
	{
		// this where the POST request to PushBots is made
		// when the sp clicks on it, it will open the to the jobViewBeforeAccept view
		// iterate over device tokens and check platform type to send push accordingly
		$http({
	      url: "https://api.pushbots.com/push/one",
	      method: "POST",
	      data: {"platform": devicePlatform,
	             "token": deviceToken,
	             "msg":msg,
	             "sound":"appbeep.wav",
	             "payload": {
	             		"uid":user_uid,
	             		"date":date,
	             		"time":time,
	             		"contractorState": contractorState,
	             		"userState": userState,
	             		"bidder_id": bidder_id
	             	}
	            },
	      headers: {
	      	"x-pushbots-appid": "56bb81481779592c4b8b4567",
	      	"x-pushbots-secret":"db07cb4936065b1ac75bde2fe0418156",
	        "Content-Type": "application/json"
	        }
	      }).success(function (data, status, headers, config) {
	          console.log("Success", data, status);
	      }).error(function (data, status, headers, config) {
	          console.log('error installing device on pushbots...', config);
	    });

	}
	return{
		send:this.send,
		sendSinglePush: this.sendSinglePush
	}


}]);