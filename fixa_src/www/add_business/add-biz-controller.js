'use strict';


angular.module("MyApp.controllers").controller("AddBizCtrl",
	["$scope", "$state", "Auth", "$ionicLoading", "FIREBASE_ROOT",'$http','$cordovaDialogs', '$ionicModal', '$ionicPopup','$cordovaEmailComposer',
	function ($scope, $state, Auth, $ionicLoading, FIREBASE_ROOT, $http, $cordovaDialogs, $ionicModal, $ionicPopup, $cordovaEmailComposer){

	var ref = new Firebase(FIREBASE_ROOT+'/users/');
	var user = ref.getAuth();
	$scope.user = user;
	var user_uid  = user.uid.substring(12);
	var userRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
	$scope.business = {
		name: '',
		category: '',
		rate: '',
		address: '',
		city: '',
		state: '',
		phone: '',
		website: '',
		about:'',
		services: []
	};

	var chosen = false;
	var meetsCriteria = false;


	$scope.serviceToAdd = {
		service: ''
	};

	$scope.addService = function(s) {
		$scope.business.services.push(s);
		$scope.serviceToAdd.service = '';
	};

	$ionicModal.fromTemplateUrl('add_business/business-list-modal.html', {
	   scope: $scope,
	   animation: 'slide-in-up'
	 }).then(function(modal) {
	   $scope.modal = modal;
	 });

	 $scope.choose = function(business) {
			chosen = true;
			var BBBrating = business.RatingIcons[0].Url; // get icon to find rating
			if(!BBBrating.match(/(A+|A|A-|B+|B|B-|C+)/)) { // regex to match the RatingIcons URL string
				meetsCriteria = false;
				userRef.child('unverified').set(true);
			} else {
				meetsCriteria = true;
				userRef.child('verified').set(true);
				
			}
			showPopup(meetsCriteria)
	 }


	function showPopup(bool) {
		if (bool) { // for true
			var alertPopup = $ionicPopup.alert({
			 title: 'Congratulations!',
			 templateUrl: 'add_business/criteriaMet.html',
			 okType: "button-energized no-border"
			});
		} else {
			var alertPopup = $ionicPopup.alert({
			 title: 'Sorry!',
			 template: 'add_business/criteriaNotMet.html',
			 okType: "button-energized no-border"
			});
		}
		finish(bool);

	}
	// add verification code here
	$scope.verify = function(name,city,state) {
		if (!name || !city || !state) {
			$cordovaDialogs.alert('All fields are required', 'Oops!', 'Ok');			   
		} else {
			// attempt to query the BBB API
			$http({
				url: "https://api.bbb.org/api/orgs/search?OrganizationName="+encodeURIComponent(name)+"&City="+encodeURIComponent(city)+"&StateProvince="+encodeURIComponent(state),
				method: 'GET',
				headers: {
				    "Authorization": "Bearer EUUaTCiLaJ3FZY1RwR9MY_65u0OVYgzMaBr3O_POmF2G5eOSTq-YH2Ic5AHQTG2UnYnD7ByXymxS8Y7O_4eM8T1Wv28x-Kgh_TZGvTkAskzCaAPq9bEqVXO5xc9xuENjoMwQrMTU2e5FstXrd2Z0CJ8gVuhJ5C9GMXrBwmUfDmTqLDMDTmtINauC9paPzq3iitGqwD-Y13iyJeAA5k8_DSw2E5K11DQswNLi0z2-4D1uSfX1Ut_45Th02BRgsaSEJQXydMhJ3UETRXouCZL9x181jerQxBiKnUzePbSoasXkI5HweYZhfaRo8W7nHUV7pzJYzI14guzdHH-7ZjWnjXAsi8OHxwEsDXF2HPtnU21VwBpBfoccWKAv1EDU1L4bcIZW70-ywAQ9E5jyW_h1z_9_NOvnB6iYspiOcHvsepU3-5oYJXwGAfWpoEb_s1eCehwqgjUcRag8LKxcl2wdxOSvpAe9rLJ-LK45n97BkqT-JQeBvWwg16ir789wjD9UHKAhFO3huUPWsh0wMnznQRMBWgQXft1ZeGxrcuk6haf00LELtuIsbhlyngF4C7_3n_h1YI5-hacvJfEQFMeCAGfcYxx7Mx2FYnujTqI7_2emuCKVQyvj3xHemS3QEjvZKDUFGd20VXMzf7Qd3uCrgkYQOgI"
				}			
			}).success(function (data, status, headers, config) {
          // console.log("Success", data, status);
         $scope.businesses = data.SearchResults; // make the returned array available to the scope
         $scope.modal.show();          
      }).error(function (data, status, headers, config) {
          console.log('error querying bbb...', config);
    });
		}
	}
	

	function finish(bool)
	{
		if(bool) {
			var bizObj = {name:$scope.business.name, category: $scope.business.category, 
				rate: $scope.business.rate, address:$scope.business.address, city: $scope.business.city, state: $scope.business.state,
				phone: $scope.business.phone, website: $scope.business.website, about:$scope.business.about,
				services: $scope.business.services
			};
			$ionicLoading.show({
			 template: 'Please wait...'
			});
			userRef.child('business').set(bizObj);			
			$ionicLoading.hide();
			$scope.goToDash();
			$scope.modal.hide();
		}
		
	};

	$scope.help = function() {
      var email = {
          to: 'hello@fixa.co',
          subject: 'Assistance Required with FIXA',          
          isHtml: true
        };

       $cordovaEmailComposer.open(email).then(null, function () {
         // user cancelled email
       });
    }

	$scope.goToDash = function()
	{
		$state.go("app.dashboard");
	};

}]);