'use strict'

var PhotoService = angular.module("PhotoService", []);
PhotoService.service("PhotoService",['$q', '$cordovaCamera', '$base64', 'Firebase', 'Auth',
	function ($q, $cordovaCamera, $base64, Firebase, Auth){

	// var destinationType = navigator.camera.DestinationType;
	var profile_id =  Auth.user.profile.id;
	var userRef = new Firebase('https://bucket-list-app.firebaseio.com/users/simplelogin:'+ profile_id);
	var options =   {
	    quality: 50,
	    // destinationType: Camera.destinationType.FILE_URI,
	    sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Photo Album
	    encodingType: 0     // 0=JPG 1=PNG
	};
	var count = 0;
	
	this.takePicture = function(imageData, type, jobObj){
		var options = { 
			        quality : 75, 
			        destinationType : Camera.DestinationType.DATA_URL, 
			        sourceType : Camera.PictureSourceType.CAMERA, 
			        allowEdit : true,
			        encodingType: Camera.EncodingType.JPEG,
			        targetWidth: 300,
			        targetHeight: 300,
			        popoverOptions: CameraPopoverOptions,
			        saveToPhotoAlbum: false
			    };
		    $cordovaCamera.getPicture(options).then(function(imageData) {
		        var imgURI = imageData;
		        var base64EncodedString = $base64.encode(imgURI);
		        var base64EncodedString = decodeURIComponent(base64EncodedString);
		        var decodedString = $base64.decode(base64EncodedString);
		        // var urlSafeBase64EncodedString = encodeURIComponent(base64EncodedString);
		        // console.log(decodedString);
		        userRef.child(type).update({"pic":decodedString});
		    }, function(err) {
		        // An error occured. Show a message to the user
		    });
	};
	return {
	  getPicture: function(options) {
	    var q = $q.defer();
	    
	    navigator.camera.getPicture(function(result) {
	      // Do any magic you need
	      q.resolve(result);
	    }, function(err) {
	      q.reject(err);
	    }, options);
	    
	    return q.promise;
	  },
	  takePicture: this.takePicture
	};
}]);