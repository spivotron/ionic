"use strict";

angular.module("UserRequest", []).factory('UserRequest', function($stateParams){
	/* Start TO USERREQUESTFACTORY */
	var self = this;
	if($stateParams) {
		$scope.jobKey = $stateParams.key;
	}
	self.goToAppAcceptedQuote = function(uid,date,time,bidder_id) {
		$state.go('app.acceptedQuote', {'uid':uid,'date':date, 'time':time, 'id':bidder_id});
	}

	self.accept = function(bidder_id) {
		$cordovaDialogs.confirm('Are you sure you want to accept this quote?', 'Accept/Deny Quote', 
			['Deny Quote','Accept Quote'])
	    .then(function(buttonIndex) {
	      // no button = 0, 'Accept' = 2, 'Deny' = 1 in ios. Reverse on browser
	      var btnIndex = buttonIndex;
	      console.log(btnIndex);
	      if (btnIndex == 2) {
					var bidderIDObj = new Firebase(FIREBASE_ROOT+'/users/'+uid+'/jobs/'+date+"/"+time+'/quotes/'+bidder_id);
				
					$scope.goToAppAcceptedQuote(uid,date,time,bidder_id);
					
					console.log("went to app.acceptedQuote");
						
	      } else {
	      	// update SPJobs
	      	var SPRef = new Firebase(FIREBASE_ROOT+'/users/'+bidder_id+'/business/jobs/'+$scope.jobKey);
	      	SPRef.update({'status':'denied'})
	      	console.log("didn't go to app.acceptedQuote");
	      }
	    });
	}

	return self;
	/* END TO USERREQUESTFACTORY */
});