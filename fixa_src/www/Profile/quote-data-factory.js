'use strict';

// this needs to maintain the variables that the user and sp share for each request
angular.module("QuoteData", []).factory('QuoteData', 
	['$stateParams', 'FIREBASE_ROOT', '$firebaseObject', '$q', 'RequestData',
	function($stateParams, FIREBASE_ROOT, $firebaseObject, $q, RequestData){

		var self = this;

		var getDetails= function() {
				var defer = $q.defer();
				if($stateParams) {
					var details = {jobName:"",jobDate: "", jobTime:"", proTimeAvailable:"",
					proDateAvailable:"",proQuote:"", proDetails:"", proDeviceToken:"", 
					proDevicePlatform: "", bidder_id: "", proUserDetails: "", jobPics:""};
					var uid  = $stateParams.uid; // user specific information
					var date = $stateParams.date;
					var time = $stateParams.time;
					var jobKey = $stateParams.key;
					var bidder_id = $stateParams.id;

					details.jobDate = date;
					details.jobTime = time;
					details.bidder_id = bidder_id;
					

					var quotes = new Firebase(FIREBASE_ROOT + '/users/'+uid+'/jobs/'+date+'/'+time+'/'+jobKey+"/quotes/"+bidder_id);				
					var quoteObj = $firebaseObject(quotes);

					// get common data like name, id and stuff
					RequestData.getDetails().then(function(data){
						details.jobName = data.jobName; // populate the view with the job name
						details.jobDetails = data.jobDetails;
						details.jobRef = data.jobRef;
						details.requester_name = data.requester_name;
						details.requester_id = data.requester_id;
						details.requester_profile_pic = data.profile_pic;	
						details.jobPics = data.jobPics;					
					}).then(function(){
						quoteObj.$loaded(function(data){
							details.proQuote = data.newQuote;
							details.proDetails = data.quoteComments;
							details.proDateAvailable = data.dateAvailable;
							details.proTimeAvailable = data.timeAvailable;
							var bidderRef = new Firebase(FIREBASE_ROOT+"/users/"+bidder_id);
							var bidderObjRef = $firebaseObject(bidderRef).$loaded(function(bidder_data){	
								details.proUserDetails = bidder_data;																				
								defer.resolve(details);
							})
							
							// if (data.pic.length) {
							// 	for (var i = 0; i < data.pic.length; i++) {
							// 		self.decodedStrings.push(data.pic[i]);
							// 	}
							// }
							
						});
					})
					
				} else {
					defer.reject("Error");
				}
			return defer.promise;
		}
	return {
		getDetails:getDetails
	}

}]);