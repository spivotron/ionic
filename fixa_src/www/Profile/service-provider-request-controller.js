'use strict';

angular.module('MyApp.controllers').controller('ServiceProviderRequest',
	['$scope', 'Firebase', 'FIREBASE_ROOT', '$firebaseObject', '$ionicLoading', 'RequestData', 
	'$http', '$timeout', '$ionicModal', '$q', '$cordovaDialogs',
  function($scope, Firebase, FIREBASE_ROOT, $firebaseObject, $ionicLoading, RequestData, 
  	$http, $timeout, $ionicModal, $q, $cordovaDialogs){

  	var ref = new Firebase(FIREBASE_ROOT+'/users/');
  	var user = ref.getAuth();
  	var userObj = new Firebase(FIREBASE_ROOT + '/users/' + user.uid.substring(12));
  	var userRefObj = $firebaseObject(userObj);

  	userRefObj.$loaded(function(data){
  		$scope.contractorStatus = data.contractorStatus; // works because $scope is available to the DOM
  		$scope.creditsAvailable = data.business.credits;
  	});
  	$scope.requesterObj = {
  		name:"",
  		id:""
  	}
  	var requestData = RequestData.getDetails().then(function(data){
  		$scope.requesterObj.name = data.requester_name;
  		$scope.requesterObj.id = data.requester_id;
  		$scope.requesterObj.profile_pic = data.requester_profile_pic;
  		$scope.requesterObj.deviceToken = data.deviceToken;
  		$scope.jobName = data.jobName;
  		$scope.jobDetails = data.jobDetails;
  		$scope.jobRef = data.jobRef;  	
  		$scope.jobPics = data.jobPics;
  		$scope.date = data.date;
  		$scope.time = data.time;
  		$scope.jobKey = data.jobKey;
  	});
  

		// start send quote
		var allQuotes = {};
		$scope.quote_info = {
			bidder: "",
			dateAvailable:"",
			timeAvailable:"",
			newQuote:"",
			quoteComments:""
		}

		function checkCredits(q) {
			// check if the credits available and resolve with number to deduct
			var defer = $q.defer();
			if($scope.creditsAvailable == 0){
				// pop up says that you must buy credits to send a quote
				$cordovaDialogs.alert('You must buy credits to bid on this job', 'Buy credits', 'Ok')
				   .then(function() {
				     // callback success
				     defer.reject();
				   });				
			} else if(q <= 200 && $scope.creditsAvailable >=1){
				defer.resolve('1'); // deduct 1 credit from the bidder's account
			} else if(q > 200 && q <= 600 && $scope.creditsAvailable >= 2) {
				defer.resolve('2');
			} else if(q > 600 && q <= 1500 && $scope.creditsAvailable >= 5) {
				defer.resolve('5');
			} else if(q > 1500 && q <= 3100 && $scope.creditsAvailable >= 15) {
				defer.resolve('15');
			} else if(q > 3100 && $scope.creditsAvailable >= 30){
				defer.resolve('30');
			} else {
				defer.reject();
			}

			return defer.promise;

			/*
				< 200, 1 credit min,
				>= 200 < 600, 2 credits min,				
				>= 600 < 1500, 5 credits min,
				>= 1500 < 3100, 15 credits min,
				>= 3100, 30 credits min
			*/		

			
			

		}



		$scope.sendQuote = function(d,t,q,c) {
			var check = checkCredits(q);
			$ionicLoading.show({
				template: '<i class="ion-loading-c"></i><br>Please wait...',
				noBackdrop: false
			});
			var bidderID = user.uid.substring(12);
			
			$scope.quote_info.bidder = bidderID; // stay 			

			$scope.quote_info.quoteComments = c; // stay
			allQuotes[bidderID] = $scope.quote_info; // stay			

			check.then(function(num){
				// num is amount of credits to deduct
				$scope.creditsAvailable -= num;
				userObj.child('business').update({'credits':$scope.creditsAvailable});
				$scope.jobRef.child('quotes').update(allQuotes);
				$scope.jobRef.update({'currentBid':q});

				// update SPJobs data
				userObj.child('business').child('jobs').child($scope.jobKey).update({'currentBid':q})
				// allQuotes = {};
				$ionicLoading.show({
					template: '<i class="ion-checkmark" style="font-size:28px;"><br /><h4>Quote Submitted!</h4></i>',
					noBackdrop: true
				});

				// get device token of requester from userRefObj
				// send notification to requester
				var deviceToken = $scope.requesterObj.deviceToken;
			$http({
		      url: "https://api.pushbots.com/push/one",
		      method: "POST",
		      data: {"platform": 0,
		             "token":deviceToken,
		             "msg":"New Quote Received!",
		             "sound":"appbeep.wav",
		             "payload": {
		             		"date": $scope.date,
		             		"time": $scope.time,
		             		"bidder_id": bidderID,
		             		"userState": "userJobView",
		             		"key":$scope.jobKey
		             	}
		            },
		      headers: {
		      	"x-pushbots-appid": "56bb81481779592c4b8b4567",
		      	"x-pushbots-secret":"db07cb4936065b1ac75bde2fe0418156",
		        "Content-Type": "application/json"
		        }
		      }).success(function (data, status, headers, config) {
		          console.log("Success", data, status);
		      }).error(function (data, status, headers, config) {
		          console.log('error installing device on pushbots...', config);
		    });
		    $ionicLoading.hide();
			}, function(error){
				console.log("failed");
				$cordovaDialogs.alert('You must buy more credits to bid on this job', 'Buy credits', 'Ok');
				$ionicLoading.hide();
			})

		}
		
		// start modal 

		$scope.showImages = function(index) {
			$scope.activeSlide = index;
			$scope.showModal("Profile/image-popover.html");
		}
		
		$scope.showModal = function(templateUrl) {
			$ionicModal.fromTemplateUrl(templateUrl, {
				scope: $scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				$scope.modal = modal;
				$scope.modal.show();
			});
		}
		
		// Close the modal
		$scope.closeModal = function() {
			$scope.modal.hide();
			$scope.modal.remove()
		};

		// end modal



}]);