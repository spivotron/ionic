'use strict';

// this needs to maintain the variables that the user and sp share for each request
angular.module("RequestData", []).factory('RequestData', [
	'$stateParams', 'FIREBASE_ROOT', '$firebaseObject', '$q',
	function($stateParams, FIREBASE_ROOT, $firebaseObject, $q){

		var self = this;
	
		var getDetails= function() {
			var defer = $q.defer();
			if($stateParams) {
				var details = {requester_name:"", requester_id:"", requester_profile_pic:"", 
				jobName:"", jobDetails:"", jobRef:"", deviceToken:"",date:"", time:"", jobPics:""};
				var uid  = $stateParams.uid; // from the URL
				var date = $stateParams.date;
				var time = $stateParams.time;
				var jobKey = $stateParams.key;
				details.date = date;
				details.time = time;
				var jobRef = new Firebase(FIREBASE_ROOT + '/users/'+uid+'/jobs/'+date+'/'+time+"/"+jobKey);
				var jobRef_with_time_for_contractor = $firebaseObject(jobRef);
				console.log(jobRef_with_time_for_contractor)

				jobRef_with_time_for_contractor.$loaded(function(data){					
					details.jobName = data.name; // populate the view with the job name
					details.jobDetails = data.details;
					details.jobRef = jobRef;
					console.log(details.jobRef);
					details.jobKey = jobKey;	
					details.jobPics = data.pic;		
					var requester_id= data.requesterID;
					var requesterRef = new Firebase(FIREBASE_ROOT+"/users/"+requester_id);
					var requesterObjRef = $firebaseObject(requesterRef).$loaded(function(requester_data){
						details.requester_name = requester_data.name;
						details.requester_id = requester_id;
						details.requester_profile_pic = requester_data.profile_pic;
						details.deviceToken = requester_data.device.deviceToken;
						defer.resolve(details);
					})
				});				
			} else {
				defer.reject("Error");
			}
		return defer.promise;
		}
	return {
		getDetails:getDetails
	}

}]);