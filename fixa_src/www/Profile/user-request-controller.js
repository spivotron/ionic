"use strict";

angular.module("MyApp.controllers").controller('UserRequest', 
	['$cordovaDialogs','QuoteData', '$scope', 'FIREBASE_ROOT', '$state', '$http', 'SendService', '$ionicModal',
	function($cordovaDialogs, QuoteData, $scope, FIREBASE_ROOT, $state, $http, SendService, $ionicModal){

	$scope.requesterObj = {};
	var requestData = QuoteData.getDetails().then(function(data){

		$scope.requesterObj.name = data.requester_name;
		$scope.requesterObj.id = data.requester_id;
		$scope.requesterObj.profile_pic = data.requester_profile_pic;
		$scope.jobName = data.jobName;
		$scope.jobDetails = data.jobDetails;
		$scope.jobRef = data.jobRef;
		$scope.jobPics = data.jobPics;
		$scope.proUserDetails = data.proUserDetails;		
		$scope.proPic = $scope.proUserDetails.profile_pic;
		$scope.proBusinessName = $scope.proUserDetails.business.name;
		$scope.proBusinessRating = $scope.proUserDetails.business.rating;

		$scope.proDateAvailable = data.proDateAvailable;
		$scope.proTimeAvailable = data.proTimeAvailable;
		$scope.proQuote = data.proQuote;
		$scope.proDeviceToken = data.proUserDetails.device.deviceToken;
		$scope.proDevicePlatform = data.proUserDetails.device.platform;
		$scope.bidder_id = data.bidder_id;

		$scope.jobDate = data.jobDate;
		$scope.jobTime = data.jobTime;

  });


	function goToAppAcceptedQuote(uid,date,time,bidder_id) {
		$state.go('app.acceptedQuote', {'uid':uid,'date':date, 'time':time, 'id':bidder_id});
	}

	$scope.accept = function() {
		var uid = $scope.requesterObj.id;
		var date = $scope.jobDate;
		var time = $scope.jobTime;
		var bidder_id = $scope.bidder_id;	
		$cordovaDialogs.confirm('Are you sure you want to accept this quote?', 'Accept/Deny Quote', 
			['Deny','Accept'])
	    .then(function(buttonIndex) {
	      // no button = 0, 'Accept' = 2, 'Deny' = 1 in ios. Reverse on browser
	      var btnIndex = buttonIndex;
	      if (btnIndex == 2) {
	      	$scope.jobRef.update({'status':"closed"}); // works!
	      	// var bidderRef = new Firebase(FIREBASE_ROOT +'/users/'+ bidder_id+'/business/');
	      	// bidderRef.update({"credits"}). will come back to later
	      	SendService.sendSinglePush(uid, date, time, "Congratulations! Your quote was accepted!", $scope.proDeviceToken, $scope.proDevicePlatform, 'acceptedQuote');
			goToAppAcceptedQuote(uid,date,time,bidder_id);
	      } else {
      		SendService.send(null, null, null, "Sorry! Your quote was not accepted", $scope.proDeviceToken, $scope.proDevicePlatform, null, null);
      		console.log("didn't go to app.acceptedQuote");
	      }
	    });
	}

	// start modal 

	$scope.showImages = function(index) {
		$scope.activeSlide = index;
		$scope.showModal("Profile/image-popover.html");
	}
	
	$scope.showModal = function(templateUrl) {
		$ionicModal.fromTemplateUrl(templateUrl, {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
	}
	
	// Close the modal
	$scope.closeModal = function() {
		$scope.modal.hide();
		$scope.modal.remove()
	};

	// end modal

}]);