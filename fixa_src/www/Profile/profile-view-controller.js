'use strict';

angular.module('MyApp.controllers').controller('ProfileViewCtrl', 
  ['$stateParams','$scope', 'Firebase', '$http', '$q', '$firebaseObject', 'FIREBASE_ROOT', '$state', '$firebaseArray',
  function ($stateParams, $scope, Firebase, $http, $q, $firebaseObject, FIREBASE_ROOT, $state, $firebaseArray){
    
    var id = $stateParams.id;
    $scope._id= id;
    var contractor_fullId= "simplelogin:"+id;
    var ref = new Firebase(FIREBASE_ROOT+'/users/');
  	var user = ref.getAuth();
    $scope.currentUserID  = user.uid.substring(12);
    
    
    var userRef = new Firebase(FIREBASE_ROOT+'/users/'+ id);
    var user_uid = user.auth.uid.substring(12);

    $scope.user = $firebaseObject(userRef);
    $scope.user.$loaded(function(data){
       $scope.profile_pic = data.profile_pic;
       $scope.businessAbout = data.business.about;       
    });

    // gets the skills number and reviews from firebase
    var skillRef = new Firebase(FIREBASE_ROOT+'/users/'+id+'/skills');
    $scope.skills = $firebaseObject(skillRef);
    $scope.skillsNumber = 0;
    skillRef.once("value", function(allSkills){
      allSkills.forEach(function(skill){
        var key = skill.key();
        if(key != null || key != 'undefined') {
          $scope.skillsNumber +=1;
        }
      })      
    });

    var reviewRef = new Firebase(FIREBASE_ROOT+'/users/'+id+'/business/ratings/');
    $scope.reviews = $firebaseArray(reviewRef);
    $scope.reviewNumber = 0;
    reviewRef.once("value", function(reviews){
      $scope.reviewNumber = reviews.numChildren();
    })

    var msg_ref = new Firebase(FIREBASE_ROOT+'/users/'+user_uid+'/messageChannels');
    var msg_ref_obj = $firebaseObject(msg_ref);
    var chat_root = user.uid+ '-'+contractor_fullId;
    var chat_ref = new Firebase(FIREBASE_ROOT+'/chat/'+chat_root+'/');
    var chat = new Firechat(chat_ref);
          
    $scope.found = false;
    $scope.roomId = null;


    $scope.gotoChatRoom = function(){
      msg_ref_obj.$loaded().then(function(channels){ 
        console.log('channels', channels);
        channels.forEach(function(channel){
          console.log('channel is ', channel);
          console.log('user is ', user.uid);
          console.log('contractor is ', contractor_fullId);
          if (channel.user1 && channel.user2 && user.uid === channel.user1 && contractor_fullId === channel.user2){
            $scope.found = true;
            $scope.roomId = channel.roomId;
          }
        });

        console.log('have we found the channel ?', $scope.found);

        if (!$scope.found){ // if not found, then crate new chat room
          chat.setUser(user.uid, user.uid, function(chat_user) {
            console.log('chat_user is ', chat_user);
            chat.getRoomList(function(data){
              console.log('room list is ', data);
            })
            
            //If there's no corresponding chat room, create one
            chat.createRoom(chat_root, 'public', function(roomId){
              console.log('room id is', roomId);
              ref.child(user_uid).child("messageChannels").child(chat_root).update({root: chat_root, roomId: roomId, user1: user.uid, user2:contractor_fullId});
              ref.child(id).child("messageChannels").child(chat_root).update({root: chat_root, roomId: roomId, user1: user.uid, user2:contractor_fullId});
              chat.inviteUser(contractor_fullId, roomId);
              chat.enterRoom(roomId);
              /*
              chat.sendMessage(roomId, 'test1', 'default', function(result){
                console.log('watch', result);
              })
              */
              $state.go('app.message', {rootId:chat_root, roomId: roomId});
            });

          });
        } else { // if chat room is already created.
          chat.setUser(user.uid, user.uid, function(chat_user) {
            console.log('chat_user is ', chat_user);
            chat.enterRoom($scope.roomId);
            /*
            chat.sendMessage($scope.roomId, 'test2', 'default', function(result){
                console.log('watch', result);
            });
            */
            $state.go('app.message', {rootId:chat_root, roomId: $scope.roomId});
          });  
        }
        
      });
     
    }// end gotoChatRoom
}]);