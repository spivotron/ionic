'use strict';

angular.module('MyApp.controllers').controller('ProfileCtrl', ['$scope', '$routeParams', 'Auth', 'User', 
	'PhotoService','$firebase', '$http', '$cordovaCamera','$cordovaGeolocation', '$cordovaFileTransfer', 
		'$base64', '$firebaseArray','$firebaseAuth', 'FIREBASE_ROOT', '$stateParams', 
		'$firebaseObject', '$filter', '$ionicLoading', '$timeout', '$state', '$ionicModal',
		'$cordovaDialogs', 
		function($scope, $routeParams, Auth, User, PhotoService, $firebase, 
		$http, $cordovaCamera,$cordovaGeolocation, $cordovaFileTransfer, 
		$base64, $firebaseArray,$firebaseAuth, FIREBASE_ROOT, $stateParams, 
		$firebaseObject, $filter, $ionicLoading, $timeout, $state, $ionicModal, 
		$cordovaDialogs) {

	/*STAYS IN PROFILECONTROLLER */
	var ref = new Firebase(FIREBASE_ROOT);
	var user = ref.getAuth();

	if (user) {
		$scope.user = user;
		var user_uid  = user.uid.substring(12);
		$scope.uid = user_uid;
		var userRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid);
		var userRefObj = $firebaseObject(userRef);
		$scope.userObj = userRefObj;
		console.log($scope.userObj)
		// load user data
		userRefObj.$loaded(
			function(data) {
				$scope.contractorStatus  = data.contractorStatus;
				$scope.profile_pic = data.profile_pic;

				$scope.profile_email = data.email;
				$scope.profile_pic = data.profile_pic;
				$scope.user_name = data.name;
				$scope.user.first_name = data.first_name;
				$scope.user.last_name  = data.last_name;
				$scope.user.phone_number = data.phone_number;
			}
		);
	};
	

	$scope.emailChangeObj = {
		oldEmail:'',
		newEmail: '',
		newEmailPassword:''
	}
	$scope.passwordChangedObj = {
		newPassword:'',
		oldPassword:''
	}

  $scope.visible = false;
	$scope.edit_off = 1;

	$scope.edit = function()
	{
		$scope.edit_on = 1;
		$scope.edit_off = 0;
	};

	// called when the user clicks save at the top of the profile edit page
	$scope.save = function()
	{
		

	if($scope.user.phone_number){
		var userObj = ref.child("users").child(user_uid);
		userObj.update({'phonenumber': $scope.user.phone_number});
	}
	if($scope.emailChangeObj.newEmail && $scope.emailChangeObj.newEmailPassword) {
		ref.changeEmail({
		  oldEmail: $scope.emailChangeObj.oldEmail,
		  newEmail: $scope.emailChangeObj.newEmail,
		  password: $scope.emailChangeObj.newEmailPassword
		}, function(error) {
		  if (error) {
		    switch (error.code) {
		      case "INVALID_PASSWORD":
		        console.log("The specified user account password is incorrect.");
		        $cordovaDialogs.alert('Your password is incorrect', 'Incorrect password', 'OK');

		        break;
		      case "INVALID_USER":
		        console.log("The specified user account does not exist.");
		        $cordovaDialogs.alert('The account does not exist', 'Account does not exist', 'OK');
		        break;
		      default:
		        console.log("Error creating user:", error);
		    }
		  } else {
		  	userRef.update({'email':$scope.emailChangeObj.newEmail});
		    $ionicLoading.show({
		    	template: '<i class="ion-checkmark" style="font-size:28px;"><br /><h4>Profile Saved!</h4></i>',
		    	noBackdrop: true
		    });
		  }
		});
	}
	if($scope.passwordChangedObj.oldPassword && $scope.passwordChangedObj.newPassword && $scope.profile_email) {
		ref.changePassword({
		  email: $scope.passwordChangedObj.email,
		  oldPassword: $scope.passwordChangedObj.oldPassword,
		  newPassword: $scope.passwordChangedObj.newPassword
		}, function(error) {
		  if (error) {
		    switch (error.code) {
		      case "INVALID_PASSWORD":
		        console.log("The specified user account password is incorrect.");
		        $cordovaDialogs.alert('Your password is incorrect', 'Incorrect Password', 'OK');
		        break;
		      case "INVALID_USER":
		        console.log("The specified user account does not exist.");
		        $cordovaDialogs.alert('The account does not exist', 'Account does not exist', 'OK');
		        break;
		      default:
		        console.log("Error changing password:", error);
		    }
		  } else {
		    $scope.passwordChangedObj.oldPassword = '';
		    $scope.passwordChangedObj.newPassword = '';

		  }
		});
	}

	$state.go('app.profile');

} // end save()

$scope.takePicture = function() {
  var options = {
      quality : 75,
      destinationType : Camera.DestinationType.DATA_URL,
      sourceType : Camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 300,
      targetHeight: 300,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false
  };

  $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.imgURI = imageData;
      var base64EncodedString = $base64.encode($scope.imgURI);
      var base64EncodedString = decodeURIComponent(base64EncodedString);
      var decodedString = $base64.decode(base64EncodedString);
      // var urlSafeBase64EncodedString = encodeURIComponent(base64EncodedString);
      // console.log(decodedString);
      userRef.child("profile_pic").set(decodedString);
  }, function(err) {
      // An error occured. Show a message to the user
  });
}
	
	/* END STAYS IN PROFILECONTROLLER */

	function locate_user(){
		$cordovaGeolocation
			.getCurrentPosition()
        .then(function (position) {
          $scope.lat  = position.coords.latitude;
          $scope.lng = position.coords.longitude;
          userRef.child("latest_location").set({
            lat: $scope.lat,
            lng: $scope.lng
          });
        }, function(err) {
          // error
          console.log("Location error!");
          console.log(err);

      });

  };
	
	
}]);
