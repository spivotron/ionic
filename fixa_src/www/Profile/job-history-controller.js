'use strict';


angular.module('MyApp.controllers').controller('JobHistoryCtrl', 
	['FIREBASE_ROOT', '$scope', '$firebaseArray', '$firebaseObject','$state', '$stateParams',
	function(FIREBASE_ROOT, $scope, $firebaseArray, $firebaseObject, $state, $stateParams){

		var ref = new Firebase(FIREBASE_ROOT);
		var user = ref.getAuth();
		$scope.jobs = [];
		$scope.quotes = [];
		var jobsRef;
		if(user) {
			$scope.user_uid = user.auth.uid.substring(12);
			var userRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.user_uid);
			var userObj = $firebaseObject(userRef);
			userObj.$loaded(function(data){
				$scope.contractorStatus = data.contractorStatus;
				if($scope.contractorStatus) {
					$scope._title = "My Job History";
					// get SPJobs
					jobsRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.user_uid+'/business/jobs');	
				} else {
					$scope._title = "My Requests";
					// load user jobs
					jobsRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.user_uid+'/jobs/');	
				}

				if(!$stateParams.name) {									
					getJobs(jobsRef);
				} else {				
					$scope.jobName = $stateParams.name;
					$scope.jobTime = $stateParams.time;
					$scope.jobDate = $stateParams.date;
					$scope.jobKey = $stateParams.key;
					jobsRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.user_uid+'/jobs/'+$scope.jobDate+'/'+$scope.jobTime+'/'+$scope.jobKey);
					var jobObject = $firebaseObject(jobsRef);
					jobObject.$loaded(function(jobData){
						$scope.jobStatus = jobData.status;
					}).then(function(){
						jobsRef = new Firebase(FIREBASE_ROOT+'/users/'+$scope.user_uid+'/jobs/'+$scope.jobDate+'/'+$scope.jobTime+'/'+$scope.jobKey+'/quotes');
						getJob(jobsRef, $scope.jobKey);
					})				
				}	
			})	

		}	  		
		
		function getJobs(jobRef) {					
			jobRef.on('value', function(snapshot){					
				snapshot.forEach(function(child){
					$scope.jobKey = child.key();
					if($scope.contractorStatus) {
						//$scope.jobs.push(child);
						var singleJob = {
							key:child.key(),
							val: child.val()										
						}
							$scope.jobs.push(singleJob);		
					} else {
						console.log(child.val());
						child.forEach(function(job){	
							job.forEach(function(d){
								var singleJob = {
								key: d.key(),
								name:d.child('name').val(),						
								quotes: d.child('quotes').numChildren(),
								status: d.child('status').val(),
								time: d.child('time').val(),
								date: d.child('date').val(),						
							}
								$scope.jobs.push(singleJob);		
							})
													
						})
					}										
				})
			});

		}		

		function getJob(jobRef,key) {
			var keyString = key.toString()
			var quote = {
				key: keyString,
				quoteData : $firebaseArray(jobRef)
			}

			$scope.quotes.push(quote);
			// $scope.quotes.$loaded(function(){	
			// 	console.log($scope.quotes.length);				
			// 	if(data.bidder) {					
			// 		var bidderRef = new Firebase(FIREBASE_ROOT+'/users/'+data.bidder+'/business/');
			// 		bidderRef.$loaded(function(business_data){
			// 			$scope.business_name = business_data.name;
			// 		});					
			// 		$scope.bidder_name = $scope.business_name;
			// 		var fullQuote = {bidder_name:data.newQuote};
			// 		$scope.quotes.push(fullQuote)
			// 	}
			// });					
		}

		$scope.goToJob = function(job) {		
			console.log("status is ", job)	
			if($scope.contractorStatus){
				if(job.val.status == 'closed') {
					$state.go('app.acceptedQuote', {'uid':job.val.requesterID, 'date':job.val.date, 'time':job.val.time, 'id':$scope.user_uid, 'key':job.key});
				} else {
					$state.go('app.serviceProviderJobView', {'uid':job.val.requesterID, 'name': job.val.name,'date':job.val.date, 'time': job.val.time, 'key':job.key});
				}				
			} else {				
				$state.go('app.quotes', {'uid':$scope.user_uid, 'name': job.name,'date':job.date, 'time': job.time, 'key':job.key});
			}
		}

		$scope.goToQuote = function(quote,id) {			
			if($scope.jobStatus == 'closed') {
				$state.go('app.acceptedQuote', {'uid':$scope.user_uid, 'date':$scope.jobDate, 'time':$scope.jobTime, 'id':id, 'key':quote.key});
			} else {
				$state.go('app.userJobView', {'uid':$scope.user_uid, 'date':$scope.jobDate, 'time':$scope.jobTime, 'id':id, 'key':quote.key});	
			}
			
		}

}]);