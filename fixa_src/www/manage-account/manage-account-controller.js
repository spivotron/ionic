'use strict';

angular.module("MyApp.controllers").controller("ManageAcctCtrl", ['$scope', 'Auth', 'Firebase', '$cordovaDialogs', '$firebase',
	function ($scope, Auth, Firebase, $cordovaDialogs, $firebase, $firebaseAuth){

	var ref = new Firebase('https://bucket-list-app.firebaseio.com/');
	var user = ref.getAuth();
	$scope.user = user;
    var user_uid  = user.uid.substring(12);
    var userRef = new Firebase("https://bucket-list-app.firebaseio.com/users/"+user_uid);
	userRef.on("value", function(snapshot) {
	  var user = snapshot.val();
	  $scope.user = user;
	  $scope.contractorStatus = user.contractorStatus;
	});
	$scope.change = function(){
		$cordovaDialogs.confirm('Are you sure you want to change your account type?', 'Change Account Type', ['Yes','Cancel'])
			.then(function(buttonIndex) {
	        // no button = 0, 'OK' = 1, 'Cancel' = 2
			var btnIndex = buttonIndex;
			//btnIndex == 1 ? userRef.update($scope.user) : continue;
			if (btnIndex == 1) {
				userRef.child("contractorStatus").set($scope.user.contractorStatus);
				$scope.biz_stuff = {_name:'', _category:'', _rate:'', _address:'', _phone:'', _website:'',_about:''};
				userRef.child('/business').set({name:$scope.biz_stuff._name,category:$scope.biz_stuff._category, rate: $scope.biz_stuff._rate, 
				    address: $scope.biz_stuff._address, phone:$scope.biz_stuff._phone, website:$scope.biz_stuff._website, about:$scope.biz_stuff._about });
			};
			
		});
	};

}]);