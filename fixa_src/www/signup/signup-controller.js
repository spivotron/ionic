'use strict';

angular.module('MyApp.controllers').controller("SignupCtrl",
  function($scope, $q, $state, $ionicLoading, $rootScope, Auth, User, Signup, $cordovaDialogs, $cordovaPush, $firebase) {
    var password;

    $scope.user = {
      email: '',
      password: '',
      contractorStatus: false,
      name: '',
      phonenumber: '',
      zip: ''
    };
    var password = $scope.user.password;
    $scope.errorMessage = null;

    $scope.signup = function() {
      $scope.errorMessage = null;

      $ionicLoading.show({
        template: 'Please wait...'
      });
      if (checkPassword($scope.user.password, $scope.user.password2) == 0)
      {
          $state.go('signup');
          $ionicLoading.hide();
      }
      else
      {
        createAuthUser()
        // login()
      }
       
    };

    function createAuthUser() {
      password = $scope.user.password;
      return Auth.createUser($scope.user.email, password, $scope.user.name, $scope.user.phonenumber, $scope.user.zip);
    };

    function checkPassword(password1, password2)
    {

        if (password1 != password2)
        {
          $cordovaDialogs.alert("Your passwords didn't match!", 'Password mismatch', 'OK');
          return 0;
        }
    };

    function sendPasswordResetEmail(authUser) {
      var defer = $q.defer();

      Auth.sendPasswordResetEmail(authUser.email).then(function() {
        defer.resolve(authUser);
      });

      return defer.promise;
    }

    function login() {
      return Auth.login($scope.user.email, $scope.user.password);
    }

    function goToChangePassword() {
      $ionicLoading.hide();
      $state.go('change-password');
    }

    function handleError(error) {
      switch (error.code) {
        case 'INVALID_EMAIL':
          $scope.errorMessage = 'Invalid email';
          break;
        case 'EMAIL_TAKEN':
          $scope.errorMessage = 'Email already exists';
          break;
        default:
          $scope.errorMessage = 'Error: [' + error.code + ']';
      }

      $ionicLoading.hide();
    }
  });
