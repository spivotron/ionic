'use strict';


var LocationService = angular.module('LocationService', []);
LocationService.factory('LocationService', function ($cordovaGeolocation, $firebase, $rootScope, 
  $http, Auth, $ionicLoading) {
 
  // make use of the http angular service to retrieve stuff from the DB. Is this asynchronous? 
  // var users_data = $http.get('https://bucket-list-app.firebaseio.com/users.json').success(gotData);
  
  var profile_id =  Auth.user.profile.id;
  // get whose signed in at a given moment by id
  // user specific reference
  var userRef = new Firebase('https://bucket-list-app.firebaseio.com/' + 'users/'+ 'simplelogin:'+ profile_id);

  

  var all_users_data = new Array();
  var all_users_size = new Array();
  var size = 0;
  var all_users = new Array();
  var fromLat,curLat,fromLng,curLng;
  var user_data = {
      async: function() {
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http.get('https://bucket-list-app.firebaseio.com/users.json').then(function (response) {
          // The then function here is an opportunity to modify the response
          console.log(response);
          // The return value gets picked up by the then in the controller.

          all_users_data.push(response.data);
        });
        // Return the promise to the controller
        return promise;
      }
    };

  function feq (f1, f2) {
    return (Math.abs(f1 - f2) < 0.000001);
   }

  // hack to animated/move the Marker class -vikrum
  // based on http://stackoverflow.com/a/10906464
  google.maps.Marker.prototype.animatedMoveTo = function(toLat, toLng) {
      $cordovaGeolocation.getCurrentPosition()
        .then(function (position){
          var fromLat = position.coords.latitude;
          var fromLng = position.coords.longitude;
          if(feq(fromLat, toLat) && feq(fromLng, toLng))
            return;
              
          // store a LatLng for each step of the animation
          var frames = [];
          for (var percent = 0; percent < 1; percent += 0.005) {
            curLat = fromLat + percent * (toLat - fromLat);
            curLng = fromLng + percent * (toLng - fromLng);
            frames.push(new google.maps.LatLng(curLat, curLng));
          }
    }); 
    /*
    move = function(marker, latlngs, index, wait) {
      marker.setPosition(latlngs[index]);
       if(index != latlngs.length-1) {
        // call the next "frame" of the animation
         setTimeout(function() { 
          move(marker, latlngs, index+1, wait); 
        }, wait);
      }
    }
      
    // begin animation, send back to origin after completion
    move(this, frames, 0, 25);
    */
  }

  function moveMarker(marker,lat,lng)
  {
    setTimeout( function(){
      marker.setPosition(new google.maps.LatLng(lat,lng));
    }, 1500);
  };

  function initialize()
  {
    $ionicLoading.show({
        content: 'Getting current location. Please wait...',
        showBackdrop: true
        });

    // This gets the user's location from the DB so the map centers on open without accepting a default LatLng object
    userRef.on("value", function(snapshot){
      var user_stuff = snapshot.val();
      var user_location = user_stuff.latest_location;
      mapOptions.center = new google.maps.LatLng(user_location.latitude,user_location.longitude)
      $ionicLoading.hide();
    })
    var mapOptions = {
      // center: new google.maps.LatLng(user_location.latitude, user_location.longitude),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.markers = {};
    return map;
  };
  initialize();
    
  function distance(lat1, lon1, lat2, lon2) {
    var R = 6371;
    var a = 
       0.5 - Math.cos((lat2 - lat1) * Math.PI / 180)/2 + 
       Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * 
       (1 - Math.cos((lon2 - lon1) * Math.PI / 180))/2;
      return R * 2 * Math.asin(Math.sqrt(a));
  };
  return {
    initialize:initialize,
    distance: distance,
    user_data:all_users_data,
    size:size,
    animatedMoveTo:google.maps.Marker.prototype.animatedMoveTo,
    moveMarker:moveMarker
    //map:map
  };
});