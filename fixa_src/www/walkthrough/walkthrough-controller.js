'use strict';


angular.module('MyApp.controllers').controller('FirstTimeCtrl', 
  ['$scope', '$state',  '$ionicLoading', '$timeout',
	function($scope, $state, $ionicLoading, $timeout) {


   $scope.endSlideShow = function() {
     window.localStorage['firstTimeUse'] = 'no';
     $state.go("home");  
   }
   $scope.slideHasChanged = function(data) {
  		if(data == 4) // final index of slides
  		{
  			$ionicLoading.show({
  			   template: 'Taking you to the Dashboard...',
  			   animation: 'fade-in',
  			   showBackdrop: true
  			 });
  			$timeout(function () {
  			    $ionicLoading.hide();
  			    $state.go("app.dashboard");
  			  }, 2000);
  			
  		}
   }

    $scope.showAlert = function() {
      $scope.alert = $ionicPopup.show({
        template: '<p>Create an account or login so we can help <b>you</b> find the <b>best</b> pro for the job.</p>' + 
               '<a class="button button-full button-clear button-positive" ng-click="close()">Ok</a>',
        scope: $scope,
        title: 'Please Login or Sign Up',
        buttons: [{
                text: "Sign Up",
                type: "button-energized",
                onTap: function(e) {
                  $state.go('signup');
                }
              }, {
                text: "Login",
                type: "button-default",
                onTap: function(e) {
                  $state.go('login');
                }
              }]
      });
     
     };
      
    $scope.close = function() {
      $scope.alert.close();
    };

}]);
