'use strict'


angular.module('MyApp.controllers').controller('SearchCtrl', ['$scope','Search', function ($scope,Search){

	var doSearch = ionic.debounce(function(query) {
	    Search.search(query).then(function(resp) {
	      console.log(query);
	      $scope.photos = resp;
	    });
	  }, 500);
	  


	  $scope.formData = {};
	  $scope.search_query = function()
	  {
	    doSearch($scope.formData.query);
	    console.log($scope.formData.query);
	        
	  }

	  $scope.search = function() {
	    $scope.search_clicked = true;
	  }
}]);