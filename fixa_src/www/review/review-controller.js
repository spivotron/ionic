"use strict";

angular.module("MyApp.controllers").controller("ReviewCTRL",
	['$scope', '$stateParams', 'FIREBASE_ROOT', '$firebaseObject', '$firebaseArray', '$q',
	function($scope, $stateParams, FIREBASE_ROOT, $firebaseObject, $firebaseArray, $q){
		
	$scope.rating = {};
  $scope.rating.rate = 0;
  $scope.rating.max = 5;
  $scope.rating.details = "";

  $scope.submit = function() {  	
  	if($stateParams)
  	{
			var id = $stateParams.id;
			var reviewRef = new Firebase(FIREBASE_ROOT+'/users/'+id+"/business/ratings");
			var businessRef = new Firebase(FIREBASE_ROOT+'/users/'+id+"/business/");
			var businessRefObj = $firebaseObject(businessRef);
			var reviewRefObj = $firebaseObject(reviewRef);	
			// adds a new rating to the ratings array
			reviewRef.push().set({'details':$scope.rating.details, 'rating':$scope.rating.rate});
			updateAverageRating(id, businessRef, reviewRef, $scope.rating.rate);
		}
  }


  function updateAverageRating(id, businessRef, reviewRef, rating) {
  	var sum = 0;
  	var businessRefObj = $firebaseObject(businessRef);
  	reviewRef.once("value", function(data){
  		var allRatings = data.numChildren();
  		if (allRatings == 1) {
  			businessRef.update({'rating':rating});
  		} else {	  			
  			// businessRefObj.$loaded(function(data){
  			// 	var newRating = data.rating + rating;
  			// 	console.log(newRating);
  			// 	var ratingtoUpdate = parseFloat(newRating)/allRatings;
  			// 	console.log(allRatings);
  			// 	// businessRef.update({'rating':ratingtoUpdate});
  			// });  


  			// sum up all ratings
  			data.forEach(function(review){
  				sum = sum + review.child('rating').val();
  				
  			})
  			console.log(sum);
  			var average = parseFloat(sum)/allRatings;
  			console.log(average);
  			// add the new rating to this average 
  			businessRef.update({'rating':average});	  			
  		}  		
  	});		
  }
 

}]);