"use strict";

angular.module("MyApp.controllers").controller("CreditCtrl", 
	['$scope', 'StripeCharge', 'Firebase', 'FIREBASE_ROOT', '$firebaseObject',
	function($scope, StripeCharge, Firebase, FIREBASE_ROOT, $firebaseObject){

	var ref = new Firebase(FIREBASE_ROOT+'/users/');
  var user = ref.getAuth();

  if (user) {
  	var user_uid = user.auth.uid.substring(12);
    var businessRef = new Firebase(FIREBASE_ROOT+"/users/"+user_uid+'/business/');
  };

	$scope.credit = {
	  title: "Buy Credits",
	  description: "Credits Roll Over Mo/Mo",
	  priceUSD: "",
	};

	$scope.status = {
	  loading: false,
	  message: "",
	};

	$scope.charge = function(amount, credits) {
		$scope.credit.priceUSD = amount;
		
	  $scope.status['loading'] = true;
	  $scope.status['message'] = "Retrieving your Stripe Token...";

	  // first get the Stripe token
	  StripeCharge.getStripeToken($scope.credit).then(
	    function(stripeToken){
	      // -->
	      proceedCharge(stripeToken, parseInt(credits));
	    },
	    function(error){
	      console.log(error)

	      $scope.status['loading'] = false;
	      if(error != "ERROR_CANCEL") {
	        $scope.status['message'] = "Oops... something went wrong";
	      } else {
	        $scope.status['message'] = "";
	      }
	    }
	  ); // ./ getStripeToken

	  function proceedCharge(stripeToken, credits) {

	    $scope.status['message'] = "Processing your payment...";

	    // then chare the user through your custom node.js server (server-side)
	    StripeCharge.chargeUser(stripeToken, $scope.credit).then(
	      function(StripeInvoiceData){
	        
	        if(StripeInvoiceData.hasOwnProperty('id')) {
	          $scope.status['message'] = "Success! Check your Stripe Account";
	        } else {
	          $scope.status['message'] = "Error, check your console";
	        };
	        $scope.status['loading'] = false;
	        console.log(StripeInvoiceData);
	        // update credit information for user
	        var businessRefObj = $firebaseObject(businessRef);
	        businessRefObj.$loaded(function(data){
	        	if(data.credits) {
	        		var totalCredits = data.credits+credits;
	        		businessRef.update({'credits':totalCredits});
	        	} else {
	        		businessRef.update({'credits':credits});
	        	}
	        })
	        


	      },
	      function(error){
	        console.log(error);

	        $scope.status['loading'] = false;
	        $scope.status['message'] = "Oops... something went wrong";
	      }
	    );

	  }; // ./ proceedCharge

	};

}]);